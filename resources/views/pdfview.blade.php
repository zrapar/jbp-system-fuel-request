<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>INVOICE </title>
        <style>
            a {
                color: #5D6975;
                text-decoration: underline;
            }

            span {
              text-transform: uppercase;
            }

            body {
                position: relative;
                width: 18.9cm;
                height: 27.7cm;
                margin: 0 auto;
                color: black;
                background: #FFFFFF;
                font-family: Arial, sans-serif;
                font-size: 12px;
            }

            header {
                padding: 10px 0;
                margin-bottom: 30px;
            }

            h1 {
                border-top: 1px solid #5D6975;
                border-bottom: 1px solid #5D6975;
                color: #5D6975;
                font-size: 2.4em;
                line-height: 1.4em;
                font-weight: normal;
                text-align: center;
                margin: 0 0 20px 0;
            }

            table {
                width: 100%;
                border-collapse: collapse;
                border-spacing: 0;
                margin-bottom: 20px;
            }

            table th,
            table td {
                text-align: center;
            }

            table th {
                padding: 5px 20px;
                color: #5D6975;
                border-bottom: 1px solid #C1CED9;
                white-space: nowrap;
                font-weight: normal;
            }

            table td {
                text-align: right;
            }

            table td.bottom {
                padding-bottom: 15px;
            }

            .footer {
                color: #5D6975;
                width: 100%;
                height: 30px;
                position: absolute;
                bottom: 0;
                padding: 8px 0;
                text-align: center;
            }

            #wave {
                position: relative;
                height: 70px;
                width: 100%;
                background: #222944;
            }
        </style>
    </head>
    <body>
        <table cellpadding="4">
            <thead>
            <tr>
                <td colspan="2" style="text-align: left">
                    <table>
                        @if(!isset($type))<tr><img src="{{public_path("images/logo.png")}}" alt="logo" style="height: auto; width: 228px" /></tr>@else<tr><img src="{{asset("images/logo.png")}}" alt="logo" style="height: auto; width: 228px" /></tr>@endif
                    </table>
                </td>

                <td colspan="3" style="text-align: right">
                    <table>
                        <tr>
                            <td>6635 W Comercial Blvd. Ste 220</td>
                        </tr>
                        <tr>
                            <td>Tamarac, FL 33319</td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 10px;">Tel: +1 (954) 220-696</td>
                        </tr>
                        <tr>
                            <td>Calle El Colegio, Edif. Centro Clave, Piso 3, Ofic. 3B</td>
                        </tr>
                        <tr>
                            <td>Sabana Grande, Caracas 1050, Venezuela</td>
                        </tr>
                        <tr>
                            <td>Tel: +58 (212) 762-9072</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center;font-size: 24px;font-weight: bold;">
                    FUEL REQUEST
                </td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="2" style="text-align: right"></td>
                <td colspan="2" style="text-align: left">
                    <table>
                        <tr>
                            <td colspan="2" style="text-align: right;font-size: 14px;"><span><b>FO#: </b></span></td>
                            <td colspan="2" style="text-align: left;font-size: 14px;"><span>{{$FuelRequest['invoice']['id_invoice']}}</span></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align:right;font-size:14px;"><span><b>Request Date: </b></span></td>
                            <td colspan="2" style="text-align:left;font-size:14px;"><span>{{\Carbon\Carbon::parse($FuelRequest['updated_at'])->format('d-M-Y')}}</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td colspan="4" style="text-align:left;font-size:14px;">
                    <span><b>TO/Para: </b> {{$FuelRequest['invoice']['suppliers']['supplier']}}</span>
                </td>
            </tr>

            <tr>
                <td colspan="2" class="bottom" style="text-align:left;font-size:14px;">
                    @if(isset($FuelRequest['invoice']['suppliers']['attn']))<span><b>ATTN: </b> {{$FuelRequest['invoice']['suppliers']['attn']}}</span> @endif
                </td>
                <td colspan="2" class="bottom" style="text-align:left;font-size:14px;">
                    @if(isset($FuelRequest['invoice']['suppliers']['cc']))<span><b>CC: </b> {{$FuelRequest['invoice']['suppliers']['cc']}}</span> @endif
                </td>
            </tr>

            <tr>
                <td colspan="4" class="bottom" style="text-align:left;font-size:14px;">
                    <span><b>FROM/De: </b> {{$FuelRequest['invoice']['agencies']['agency']}} </span>
                </td>
            </tr>

            <tr>
                <td colspan="4" class="bottom" style="text-align:justify;font-size:14px;">
                    PLEASE FUEL THE FOLLOWING JBP MARKETING INTERNATIONAL CUSTOMER:/Favor suministrar combustible al siguiente cliente de JBP Marketing International:
                </td>
            </tr>

            <tr>
                <td colspan="4" class="bottom" style="text-align:left;font-size:14px;">
                    <span><b>CUSTOMER/Cliente: </b> {{$FuelRequest['invoice']['clients']['client']}}</span>
                </td>
            </tr>

            <tr>
                <td colspan="4" class="bottom" style="text-align:left;font-size:14px;">
                    <span><b>LOCATION/Localidad: </b> {{$FuelRequest['departure']['label']}}</span>
                </td>
            </tr>

            <tr>
                <td colspan="4" class="bottom" style="text-align:left;font-size:14px;">
                    <span><b>AC REGISTRATION / Matrícula: </b> {{$FuelRequest['ac_registration']}}</span>
                </td>
            </tr>

            <tr>
                <td colspan="4" class="bottom" style="text-align:left;font-size:14px;">
                    <span><b>AIRCRAFT TYPE/Aereonave: </b> {{$FuelRequest['aircraft']['type']}}</span>
                </td>
            </tr>
            
            <tr>
                <td colspan="1" class="bottom" style="text-align:left;font-size:14px;">
                    <span><b>SCHEDULE/Itinerario: </b></span>
                    <br>
                    <br>
                </td>
                <td colspan="2" class="bottom" style="text-align:left;font-size:14px;">
                    <span><b>ETA: </b> {{\Carbon\Carbon::parse($FuelRequest['arrival_date'])->format('d-M-Y')}} TIME: {{\Carbon\Carbon::parse($FuelRequest['arrival_time'])->format('H:i')}}</span>
                    <br>
                    <span><b>ETD: </b> {{\Carbon\Carbon::parse($FuelRequest['departure_date'])->format('d-M-Y')}} TIME: {{\Carbon\Carbon::parse($FuelRequest['departure_time'])->format('H:i')}}</span>
                </td>
                
            </tr>

            <tr>
                <td colspan="4" class="bottom" style="text-align:left;font-size:14px;">
                    <span><b>DESTINATION/Destino: </b> {{$FuelRequest['arrival']['label']}}</span>
                </td>
            </tr>

            @if(isset($FuelRequest['handler']))
              <tr>
                  <td colspan="4" class="bottom" style="text-align:left;font-size:14px;">
                      <span><b>HANDLER/FBO: </b> {{$FuelRequest['handler']}}</span>
                  </td>
              </tr>
            @endif

            <tr>
                <td colspan="4" class="bottom" style="text-align:left;font-size:14px;">
                    <span><b>PRODUCT/Producto: </b> {{$FuelRequest['type_product']}}</span>
                </td>
            </tr>

            <tr>
                <td colspan="4" class="bottom" style="text-align:left;font-size:14px;">
                    <span><b>ESTIMATED UPLIFT/Carga: </b> {{$FuelRequest['gallons']}} @if($FuelRequest['gallons'] != "At Captain's request") Gallons @endif</span>
                </td>
            </tr>
            
            @if(isset($FuelRequest['remarks']))
                <tr>
                    <td colspan="4" class="bottom" style="text-align:left;font-size:14px;">
                        <span><b>REMARKS/Observaciones: </b> {{$FuelRequest['remarks']}}</span>
                    </td>
                </tr>
            @endif

            <tr>
                <td colspan="4" class="bottom" style="text-align:justify;font-size:14px;">
                    <b>Please confirm to
                        <a href="mailto:release@jbpmarketing.com?subject=Confirm Fuel"
                           style="text-decoration: underline; color: #0068A5;" title="release@jbpmarketing.com">release@jbpmarketing.com</a>&nbsp;or
                        via fax +1 (954)
                        252-3838 and invoice JBP
                        Marketing International, LLC
                        thru correnponding
                        procedure. Thank you for
                        prompt attention to this
                        request.</b>
                </td>
            </tr>

            <tr>
                <td colspan="4" class="bottom" style="text-align:left;font-size:14px;">
                    <span>Best Regards,</span>
                </td>
            </tr>

            <tr>
                <td colspan="4" class="bottom" style="text-align:left;font-size:14px;font-style:italic;">
                    <span><b>Fuel Dispatch/Ops</b></span>
                </td>
            </tr>
            </tbody>
        </table>
    </body>
</html>