import React from 'react';
import PropTypes from 'prop-types';
import { Spin, Icon } from 'antd';

const antIcon = <Icon type='loading' style={{ fontSize: 65 }} spin />;

// set display name for component
const displayName = 'CommonLoader';

// validate component properties
const propTypes = {
	isLoading: PropTypes.bool,
	error: PropTypes.object
};

const LoadingComponent = ({ isLoading, error }) => {
	// Handle the loading state
	if (isLoading) {
		return (
			<div className='container'>
				<div className='centerSping'>
					<Spin indicator={antIcon} tip='Loading, please wait...' />
				</div>
			</div>
		);
	}
	// Handle the error state
	else if (error) {
		return <div>Sorry, there was a problem loading the page.</div>;
	} else {
		return (
			<div className='container'>
				<div className='centerSping'>
					<Spin indicator={antIcon} tip='Loading, please wait...' />
				</div>
			</div>
		);
	}
};

LoadingComponent.displayName = displayName;
LoadingComponent.propTypes = propTypes;

export default LoadingComponent;
