import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

const DashboardLayout = ({ children }) => {
	// Handle the loading state
	return <Fragment>{children}</Fragment>;
};
DashboardLayout.propTypes = {
	children: PropTypes.node.isRequired,
	dispatch: PropTypes.func.isRequired
};
export default DashboardLayout;
