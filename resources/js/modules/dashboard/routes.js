// import lib
import Loadable from 'react-loadable';

// import components
import LoadingComponent from '../../common/loader/index';

export default [
	{
		path: '/dashboard',
		exact: true,
		auth: true,
		component: Loadable({
			loader: () => import('./pages/dashboard/index'),
			loading: LoadingComponent
		})
	},
	{
		path: '/dashboard/suppliers',
		exact: true,
		auth: true,
		component: Loadable({
			loader: () => import('./pages/suppliers/read/index'),
			loading: LoadingComponent
		})
	},
	{
		path: '/dashboard/supplier/create',
		exact: true,
		auth: true,
		component: Loadable({
			loader: () => import('./pages/suppliers/create/index'),
			loading: LoadingComponent
		})
	},
	{
		path: '/dashboard/supplier/edit/:uuid',
		exact: true,
		auth: true,
		component: Loadable({
			loader: () => import('./pages/suppliers/create/index'),
			loading: LoadingComponent
		})
	},
	{
		path: '/dashboard/clients',
		exact: true,
		auth: true,
		component: Loadable({
			loader: () => import('./pages/clients/read/index'),
			loading: LoadingComponent
		})
	},
	{
		path: '/dashboard/client/create',
		exact: true,
		auth: true,
		component: Loadable({
			loader: () => import('./pages/clients/create/index'),
			loading: LoadingComponent
		})
	},
	{
		path: '/dashboard/client/edit/:uuid',
		exact: true,
		auth: true,
		component: Loadable({
			loader: () => import('./pages/clients/create/index'),
			loading: LoadingComponent
		})
	},
	{
		path: '/dashboard/agencies',
		exact: true,
		auth: true,
		component: Loadable({
			loader: () => import('./pages/agencies/read/index'),
			loading: LoadingComponent
		})
	},
	{
		path: '/dashboard/agency/create',
		exact: true,
		auth: true,
		component: Loadable({
			loader: () => import('./pages/agencies/create/index'),
			loading: LoadingComponent
		})
	},
	{
		path: '/dashboard/agency/edit/:uuid',
		exact: true,
		auth: true,
		component: Loadable({
			loader: () => import('./pages/agencies/create/index'),
			loading: LoadingComponent
		})
	},
	{
		path: '/dashboard/settings',
		exact: true,
		auth: true,
		component: Loadable({
			loader: () => import('./pages/settings/index'),
			loading: LoadingComponent
		})
	}
];
