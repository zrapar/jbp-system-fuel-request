import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {
	Avatar,
	// Icon,
	// Badge,
	Row,
	Col,
	Menu,
	Dropdown,
	Typography,
	Button
} from 'antd';
import { logout } from '../../../auth/service';
import { useDispatch } from 'react-redux';

const { Text } = Typography;

const Header = ({ user }) => {
	const dispatch = useDispatch();
	const menu = (
		<Menu>
			<Menu.Item key='1'>
				<Button
					type='primary'
					icon='poweroff'
					onClick={() => dispatch(logout())}
				>
					Logout
				</Button>
			</Menu.Item>
		</Menu>
	);

	return (
		<Row type='flex' justify='end'>
			{/* <Col span={1}>
				<Badge dot={true}>
					<Dropdown
						overlay={menu}
						placement='bottomRight'
						overlayClassName='mt-1p'
					>
						<Link to='#!' className='no-decoration'>
							<Icon type='bell' style={{ fontSize: '24px' }} />
						</Link>
					</Dropdown>
				</Badge>
			</Col> */}
			<Col span={1}>
				<Avatar size='large' icon='user' />
			</Col>
			<Col span={3}>
				<Dropdown
					overlay={menu}
					placement='bottomRight'
					overlayClassName='mt-1p'
				>
					<Link className='no-decoration' to='#!'>
						<Text strong>{user.name}</Text>
					</Link>
				</Dropdown>
			</Col>
		</Row>
	);
};
Header.propTypes = {
	user: PropTypes.object.isRequired
};

export default Header;
