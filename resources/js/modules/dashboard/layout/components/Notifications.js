import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { notification } from 'antd';

import { CLOSE_DELETE_MESSAGE } from '../../../web/store/action-types';
import { CLOSE_UPDATE_MESSAGE } from '../../../dashboard/store/action-types';

const key = 'Notify';

const openNotification = (title, message, type) => {
	notification[type]({
		key,
		message: title,
		description: message,
		onClick: () => {
			notification.close(key);
		}
	});
};

const Notifications = () => {
	const dispatch = useDispatch();
	const completeForm = useSelector(
		({ fuel_request }) => fuel_request.completeForm
	);
	const deleteMessage = useSelector(
		({ fuel_request }) => fuel_request.deleteMessage
	);

	const updateSelfMessage = useSelector(
		({ dashboard }) => dashboard.updateSelf
	);

	useEffect(() => {
		if (
			completeForm &&
			// eslint-disable-next-line no-prototype-builtins
			completeForm.hasOwnProperty('success') &&
			completeForm.success
		) {
			openNotification(
				completeForm.success ? 'Success' : 'Error',
				completeForm.message,
				completeForm.success ? 'success' : 'error'
			);
			setTimeout(() => {
				dispatch({ type: CLOSE_DELETE_MESSAGE });
			}, 1000);
		}
		if (
			deleteMessage &&
			// eslint-disable-next-line no-prototype-builtins
			deleteMessage.hasOwnProperty('success') &&
			deleteMessage.success
		) {
			openNotification(
				deleteMessage.success ? 'Success' : 'Error',
				deleteMessage.message,
				deleteMessage.success ? 'success' : 'error'
			);
			setTimeout(() => {
				dispatch({ type: CLOSE_DELETE_MESSAGE });
			}, 1000);
		}
		if (
			updateSelfMessage &&
			// eslint-disable-next-line no-prototype-builtins
			updateSelfMessage.hasOwnProperty('success') &&
			updateSelfMessage.success
		) {
			openNotification(
				updateSelfMessage.success ? 'Success' : 'Error',
				updateSelfMessage.message,
				updateSelfMessage.success ? 'success' : 'error'
			);
			setTimeout(() => {
				dispatch({ type: CLOSE_UPDATE_MESSAGE });
			}, 1000);
		}
	}, [completeForm, deleteMessage, updateSelfMessage]);

	return <React.Fragment></React.Fragment>;
};

export default Notifications;
