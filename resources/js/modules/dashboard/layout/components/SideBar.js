import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router';
import { Layout, Menu, Icon } from 'antd';
import PropTypes from 'prop-types';

const { SubMenu, Item } = Menu;
const { Sider } = Layout;

const SideBar = ({ history }) => {
	let active = [];
	let activeSubMenu = [];
	useEffect(() => {
		switch (history.location.pathname) {
			case '/dashboard':
				active.push('1');
				break;
			case '/dashboard/suppliers':
				activeSubMenu.push('sub1');
				active.push('/suppliers');
				break;

			case '/dashboard/clients':
				activeSubMenu.push('sub2');
				active.push('/clients');

				break;
			case '/dashboard/agencies':
				activeSubMenu.push('sub3');
				active.push('/agencies');

				break;
			case '/dashboard/settings':
				activeSubMenu.push('sub4');
				active.push('/settings');

				break;

			default:
				active.push('1');
				break;
		}
	}, []);

	const [collapse, toggleCollapsed] = useState(false);

	const onCollapse = () => {
		toggleCollapsed(!collapse);
	};

	const onClick = item => {
		const url = item.key === '1' ? '' : item.key;
		history.push({
			pathname: `/dashboard${url}`
		});
	};
	return (
		<Sider
			collapsible
			collapsed={collapse}
			onCollapse={() => onCollapse()}
			style={{ background: '#fff' }}
		>
			<img src='images/logo.png' alt='logo' className='logo-dashboard' />

			<Menu
				theme='light'
				defaultSelectedKeys={active}
				// openKeys={activeSubMenu}
				mode='inline'
				onClick={onClick}
			>
				<Item key='1'>
					<Icon type='pie-chart' />
					<span>Fuel Request</span>
				</Item>
				<SubMenu
					key='sub1'
					title={
						<span>
							<Icon type='user' />
							<span>Supplier</span>
						</span>
					}
				>
					<Item key='/suppliers'>Ver</Item>
					<Item key='/supplier/create'>Crear</Item>
				</SubMenu>
				<SubMenu
					key='sub2'
					title={
						<span>
							<Icon type='user' />
							<span>Clientes</span>
						</span>
					}
				>
					<Item key='/clients'>Ver</Item>
					<Item key='/client/create'>Crear</Item>
				</SubMenu>
				<SubMenu
					key='sub3'
					title={
						<span>
							<Icon type='team' />
							<span>Agencias JBP</span>
						</span>
					}
				>
					<Item key='/agencies'>Ver</Item>
					<Item key='/agency/create'>Crear</Item>
				</SubMenu>
        <Item key='/settings'>
					<Icon type='setting' />
					<span>Configuraciones</span>
				</Item>
			</Menu>
		</Sider>
	);
};
SideBar.propTypes = {
	history: PropTypes.object
};
export default withRouter(SideBar);
