import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Layout } from 'antd';
import HeaderContent from './components/Header';
import SideBar from './components/SideBar';
import Notifications from './components/Notifications';
import { useSelector, useDispatch } from 'react-redux';
import { fetchUser } from '../../auth/service';

const { Header, Content, Footer } = Layout;

const DashboardLayout = ({ children }) => {
	const dispatch = useDispatch();
	const userData = useSelector(({ user }) => user);
	useEffect(() => {
		if (userData.name.length === 0) {
			dispatch(fetchUser());
		}
	}, [userData.name]);

	return (
		<Layout style={{ minHeight: '100vh' }}>
			<SideBar />
			<Layout>
				<Header style={{ background: '#fff', padding: 0 }}>
					<HeaderContent user={userData} />
				</Header>
				<Content style={{ margin: '24px 16px 0' }}>
					<div
						style={{
							padding: 24,
							background: '#fff',
							minHeight: 360
						}}
					>
						{children}
					</div>
				</Content>
				<Footer style={{ textAlign: 'center' }}>JBP SYSTEM</Footer>
				<Notifications />
			</Layout>
		</Layout>
	);
};
DashboardLayout.propTypes = {
	children: PropTypes.node.isRequired
};
export default DashboardLayout;
