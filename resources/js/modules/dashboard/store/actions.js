/* ============
 * Actions for the auth module
 * ============
 *
 * The actions that are available on the
 * auth module.
 */

import {
	GET_REQUESTS,
	GET_SUPPLIERS,
	SAVE_SUPPLIER,
	GET_CLIENTS,
	SAVE_CLIENT,
	GET_AGENCIES,
	SAVE_AGENCY,
	CLEAN_FORM,
	DELETE_ITEM,
	ERROR_MESSAGE,
	CLEAN_ERROR,
	USER_UPDATE,
	OPEN_UPDATE_MESSAGE
} from './action-types';

export const getRequests = data => {
	return { type: GET_REQUESTS, payload: data };
};
export const getSuppliers = data => {
	return { type: GET_SUPPLIERS, payload: data };
};
export const saveSupplierApi = data => {
	return { type: SAVE_SUPPLIER, payload: data };
};
export const getClients = data => {
	return { type: GET_CLIENTS, payload: data };
};
export const saveClientApi = data => {
	return { type: SAVE_CLIENT, payload: data };
};
export const getAgencies = data => {
	return { type: GET_AGENCIES, payload: data };
};
export const saveAgencyApi = data => {
	return { type: SAVE_AGENCY, payload: data };
};
export const errorMessage = data => {
	return { type: ERROR_MESSAGE, payload: data };
};
export const cleanComplete = () => {
	return { type: CLEAN_FORM };
};
export const cleanError = name => {
	return { type: CLEAN_ERROR, payload: name };
};

export const deleteItem = (res, resource, uuid) => {
	return { type: DELETE_ITEM, payload: { res, resource, uuid } };
};

export const updateSelf = res => {
	return { type: USER_UPDATE, payload: res };
};

export const updateMessage = res => {
	return { type: OPEN_UPDATE_MESSAGE, payload: res };
};
