/* eslint-disable no-mixed-spaces-and-tabs */
// import HTTP from '../../../utils/Http';
import {
	GET_REQUESTS,
	GET_SUPPLIERS,
	SAVE_SUPPLIER,
	GET_CLIENTS,
	SAVE_CLIENT,
	GET_AGENCIES,
	SAVE_AGENCY,
	CLEAN_FORM,
	DELETE_ITEM,
	ERROR_MESSAGE,
	CLEAN_ERROR,
	OPEN_UPDATE_MESSAGE,
	CLOSE_UPDATE_MESSAGE
} from './action-types';
// import { showMessage } from './actions';
import _ from 'lodash';

const initialState = {
	requests: [],
	suppliers: [],
	clients: [],
	agencies: [],
	airports: [],
	aircrafts: [],
	pagination: {
		requests: null,
		suppliers: null,
		clients: null,
		agencies: null
	},
	completeForm: null,
	deleteMessage: null,
	updateSelf: null
};

const reducer = (state = initialState, { type, payload = null }) => {
	switch (type) {
		case GET_REQUESTS:
			return getRequest(state, payload);
		case GET_SUPPLIERS:
			return getSupplier(state, payload);
		case SAVE_SUPPLIER:
			return saveSupplier(state, payload);
		case GET_CLIENTS:
			return getClient(state, payload);
		case SAVE_CLIENT:
			return saveClient(state, payload);
		case GET_AGENCIES:
			return getAgency(state, payload);
		case SAVE_AGENCY:
			return saveAgency(state, payload);
		case CLEAN_FORM:
			return cleanMessage(state);
		case DELETE_ITEM:
			return deleteItem(state, payload);
		case ERROR_MESSAGE:
			return errorMessage(state, payload);
		case CLEAN_ERROR:
			return errorClean(state, payload);
		case OPEN_UPDATE_MESSAGE:
			return openMessageUser(state, payload);
		case CLOSE_UPDATE_MESSAGE:
			return closeMessageUser(state, payload);

		default:
			return state;
	}
};

const getRequest = (state, payload) => {
	return {
		...state,
		requests: [...payload.data],
		pagination: {
			...state.pagination,
			requests: _.omit(payload, 'data')
		}
	};
};

const getSupplier = (state, payload) => {
	return {
		...state,
		suppliers: [...payload.data],
		pagination: {
			...state.pagination,
			suppliers: _.omit(payload, 'data')
		}
	};
};

const saveSupplier = (state, payload) => {
	return {
		...state,
		completeForm: _.omit(payload, 'data'),
		suppliers:
			payload.type === 'update'
				? state.suppliers.map(it => {
						if (it.uuid === payload.data.uuid) {
							return payload.data;
						}
						return it;
				  })
				: [...state.suppliers, payload.data]
	};
};

const getClient = (state, payload) => {
	return {
		...state,
		clients: [...payload.data],
		pagination: {
			...state.pagination,
			clients: _.omit(payload, 'data')
		}
	};
};

const saveClient = (state, payload) => {
	return {
		...state,
		completeForm: _.omit(payload, 'data'),
		clients:
			payload.type === 'update'
				? state.clients.map(it => {
						if (it.uuid === payload.data.uuid) {
							return payload.data;
						}
						return it;
				  })
				: [...state.clients, payload.data]
	};
};
const getAgency = (state, payload) => {
	return {
		...state,
		agencies: [...payload.data],
		pagination: {
			...state.pagination,
			agencies: _.omit(payload, 'data')
		}
	};
};

const saveAgency = (state, payload) => {
	return {
		...state,
		completeForm: _.omit(payload, 'data'),
		agencies:
			payload.type === 'update'
				? state.agencies.map(it => {
						if (it.uuid === payload.data.uuid) {
							return payload.data;
						}
						return it;
				  })
				: [...state.agencies, payload.data]
	};
};

const deleteItem = (state, payload) => {
	return {
		...state,
		[payload.resource]: state[payload.resource].filter(
			it => it.uuid !== payload.uuid
		),
		completeForm: payload.res
	};
};

const cleanMessage = state => {
	return {
		...state,
		completeForm: null
	};
};

const errorMessage = (state, payload) => {
	return {
		...state,
		completeForm: payload
	};
};
const closeMessageUser = state => {
	return {
		...state,
		updateSelf: null
	};
};

const openMessageUser = (state, payload) => {
	return {
		...state,
		updateSelf: payload
	};
};
const errorClean = (state, payload) => {
	return {
		...state,
		completeForm: {
			...state.completeForm,
			errors: _.omit(state.completeForm.errors, payload)
		}
	};
};

export default reducer;
