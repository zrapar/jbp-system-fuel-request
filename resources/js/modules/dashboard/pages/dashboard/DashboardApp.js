import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Table, Button, notification, Input, Icon } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import {
	getRequestsData,
	getSuppliersData,
	getClientsData,
	getAgenciesData,
	sendMailService
} from '../../services';
import DashboardLayout from '../../layout/DashboardLayout';
import Moment from 'react-moment';
import Highlighter from 'react-highlight-words';

const openNotification = (title, message) => {
	notification.open({
		key: 'Dashboard',
		message: title,
		description: message,
		onClick: () => {
			console.log('Notification Clicked!');
		}
	});
};

const DashboardApp = () => {
	const dispatch = useDispatch();
	const requests = useSelector(({ dashboard }) => dashboard.requests);
	const pagination = useSelector(
		({ dashboard }) => dashboard.pagination.requests
	);
	const completeForm = useSelector(
		({ fuel_request }) => fuel_request.completeForm
	);

	const [state, setState] = useState({
		data: [],
		pagination: {},
		loading: true,
		searchText: ''
	});

	let searchInput = null;

	useEffect(() => {
		dispatch(getSuppliersData());
		dispatch(getClientsData());
		dispatch(getAgenciesData());
		dispatch(getRequestsData());
	}, [dispatch]);

	useEffect(() => {
		setState({
			...state,
			data: [...requests],
			pagination: {
				...pagination,
				defaultPageSize: 15,
				pageSize: 15
			},
			loading: false
		});
	}, [pagination, requests, completeForm]);

	const handleTableChange = pagination => {
		if (state.pagination.currentPage !== pagination.current) {
			setState({ ...state, loading: true });
			dispatch(getRequestsData(pagination.current));
		}
	};

	const sendMail = uuid => {
		dispatch(sendMailService(uuid));
	};

	const columns = [
		{
			title: 'Customer',
			dataIndex: 'customer',
			key: 'customer',
			sorter: (a, b) => a.customer.localeCompare(b.customer),
			width: '20%',
			filterDropdown: ({
				setSelectedKeys,
				selectedKeys,
				confirm,
				clearFilters
			}) => (
				<div style={{ padding: 8 }}>
					<Input
						ref={node => {
							searchInput = node;
						}}
						placeholder={`Search customer`}
						value={selectedKeys[0]}
						onChange={e =>
							setSelectedKeys(e.target.value ? [e.target.value] : [])
						}
						onPressEnter={() => handleSearch(selectedKeys, confirm)}
						style={{ width: 188, marginBottom: 8, display: 'block' }}
					/>
					<Button
						type='primary'
						onClick={() => handleSearch(selectedKeys, confirm)}
						icon='search'
						size='small'
						style={{ width: 90, marginRight: 8 }}
					>
						Buscar
					</Button>
					<Button
						onClick={() => handleReset(clearFilters)}
						size='small'
						style={{ width: 90 }}
					>
						Limpiar
					</Button>
				</div>
			),
			filterIcon: filtered => (
				<Icon
					type='search'
					size='large'
					style={{ color: filtered ? '#1890ff' : '#000' }}
				/>
			),
			onFilter: (value, record) =>
				record['customer']
					.toString()
					.toLowerCase()
					.includes(value.toLowerCase()),
			onFilterDropdownVisibleChange: visible => {
				if (visible) {
					setTimeout(() => searchInput.select());
				}
			},
			render: text => (
				<Highlighter
					highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
					searchWords={[state.searchText]}
					autoEscape
					textToHighlight={text.toString()}
				/>
			)
		},
		{
			title: 'Email',
			dataIndex: 'emailCostumer',
			key: 'emailCostumer',
			sorter: (a, b) => a.emailCostumer.localeCompare(b.emailCostumer),
			// eslint-disable-next-line react/prop-types
			filterDropdown: ({
				setSelectedKeys,
				selectedKeys,
				confirm,
				clearFilters
			}) => (
				<div style={{ padding: 8 }}>
					<Input
						ref={node => {
							searchInput = node;
						}}
						placeholder={`Search email`}
						value={selectedKeys[0]}
						onChange={e =>
							setSelectedKeys(e.target.value ? [e.target.value] : [])
						}
						onPressEnter={() => handleSearch(selectedKeys, confirm)}
						style={{ width: 188, marginBottom: 8, display: 'block' }}
					/>
					<Button
						type='primary'
						onClick={() => handleSearch(selectedKeys, confirm)}
						icon='search'
						size='small'
						style={{ width: 90, marginRight: 8 }}
					>
						Buscar
					</Button>
					<Button
						onClick={() => handleReset(clearFilters)}
						size='small'
						style={{ width: 90 }}
					>
						Limpiar
					</Button>
				</div>
			),
			filterIcon: filtered => (
				<Icon
					type='search'
					size='large'
					style={{ color: filtered ? '#1890ff' : '#000' }}
				/>
			),
			onFilter: (value, record) =>
				record['emailCostumer']
					.toString()
					.toLowerCase()
					.includes(value.toLowerCase()),
			onFilterDropdownVisibleChange: visible => {
				if (visible) {
					setTimeout(() => searchInput.select());
				}
			},
			render: text => (
				<Highlighter
					highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
					searchWords={[state.searchText]}
					autoEscape
					textToHighlight={text.toString()}
				/>
			)
		},
		{
			title: 'Product',
			dataIndex: 'typeProduct',
			filters: [
				{ text: 'Jet A1', value: 'Jet A1' },
				{ text: 'AV Gas', value: 'AV Gas' }
			],
			onFilter: (value, record) => record.typeProduct === value,
			filterMultiple: false
		},
		{
			title: 'Fecha de Solicitud',
			dataIndex: 'createdAt',
			defaultSortOrder: 'descend',
			sorter: (a, b) => a.id - b.id,
			sortDirections: ['descend', 'ascend'],
			render: data => (
				<Moment locale='es' format='DD-MM-YYYY HH:mm' date={data} />
			)
		},
		{
			title: 'Status',
			dataIndex: 'isCompleted',
			filters: [
				{ text: 'Completado', value: '1' },
				{ text: 'Por Revisar', value: '0' }
			],
			onFilter: (value, record) => record.isCompleted === value,
			render: data => (data === '1' ? 'Completado' : 'Por Revisar'),
			filterMultiple: false
		},
		{
			title: 'Email',
			dataIndex: 'sendMail',
			filters: [
				{ text: 'Enviado', value: '1' },
				{ text: 'Sin Enviar', value: '0' }
      ],
      width: '10%',
			onFilter: (value, record) => record.isCompleted === value,
			render: data => (data === '1' ? 'Enviado' : 'Sin Enviar'),
			filterMultiple: false
		},
		{
			title: 'Actions',
			render: data => <RowButtons data={data} />,
			className: 'text-center'
		}
	];

	const handleSearch = (selectedKeys, confirm) => {
		confirm();
		setState({ ...state, searchText: selectedKeys[0] });
	};

	const handleReset = clearFilters => {
		clearFilters();
		setState({ ...state, searchText: '' });
	};

	const RowButtons = ({ data }) => {
		if (data.isCompleted === '0') {
			return (
				<a
					target='_blank'
					rel='noopener noreferrer'
					href={`/response/${data.uuid}`}
				>
					<Button type='primary' icon='eye' />
				</a>
			);
		}

		return (
			<React.Fragment>
				{data.sendMail === '0' && (
					<Button type='primary' icon='mail' onClick={() => sendMail(data.uuid)} style={{ marginRight: 5 }} />
				)}
				<a
					target='_blank'
					rel='noopener noreferrer'
					href={`/api/order/${data.uuid}`}
					style={{ marginRight: 5 }}
				>
					<Button type='primary' icon='eye'></Button>
				</a>
				<a
					target='_blank'
					rel='noopener noreferrer'
					href={`/api/download/${data.uuid}`}
					
				>
					<Button type='secondary' icon='download'></Button>
				</a>
			</React.Fragment>
		);
	};

	RowButtons.propTypes = {
		data: PropTypes.object.isRequired
	};

	if (state.seePdf) {
		openNotification('Error', 'Ha ocurrido un error mostrando este PDF');
	}

	return (
		<DashboardLayout>
			<div
				style={{
					padding: 24,
					background: '#fff',
					minHeight: 360
				}}
			>
				<Table
					columns={columns}
					rowKey={record => record.uuid}
					dataSource={state.data}
					pagination={state.pagination}
					loading={state.loading}
					onChange={handleTableChange}
					rowClassName={record =>
						record.isCompleted === '0' ? 'nonCheck' : 'alreadyCheck'
					}
				/>
			</div>
		</DashboardLayout>
	);
};

DashboardApp.propTypes = {
	history: PropTypes.object.isRequired,
	location: PropTypes.object.isRequired
};

export default DashboardApp;
