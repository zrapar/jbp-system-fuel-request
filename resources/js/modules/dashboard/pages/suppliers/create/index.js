// import libs
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

// import components
import createSupplier from './createSupplier';

export default withRouter(connect()(createSupplier));
