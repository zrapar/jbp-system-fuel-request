// import libs
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

// import components
import Suppliers from './Suppliers';

export default withRouter(connect()(Suppliers));
