import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Table, Popconfirm, Icon, Row, Col, notification } from 'antd';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getSuppliersData, cleanComplete, deleteItem } from '../../../services';
import DashboardLayout from '../../../layout/DashboardLayout';

const Suppliers = () => {
	const dispatch = useDispatch();
	const suppliers = useSelector(({ dashboard }) => dashboard.suppliers);

	const pagination = useSelector(
		({ dashboard }) => dashboard.pagination.suppliers
	);
	const completeForm = useSelector(({ dashboard }) => dashboard.completeForm);

	const [state, setState] = useState({
		data: [],
		pagination: {},
		loading: true
	});

	useEffect(() => {
		if (pagination) {
			setState({
				...state,
				data: [...suppliers],
				pagination: {
					...pagination,
					defaultPageSize: 15,
					pageSize: 15
				},
				loading: false
			});
		} else {
			dispatch(getSuppliersData());
		}
	}, [pagination]);

	const handleTableChange = (pagination, filters, sorter) => {
		if (state.pagination.currentPage !== pagination.current) {
			setState({ ...state, loading: true });
			dispatch(getSuppliersData(pagination.current));
		}
		console.log(filters);
		console.log(sorter);
	};
	const columns = [
		{
			title: 'Supplier',
			dataIndex: 'supplier'
		},
		{
			title: "Supplier's Email",
			dataIndex: 'supplierEmail'
		},
		{
			title: 'Attn',
			dataIndex: 'attn',
			render: data => (data ? data : 'Informacion no proporcionada')
		},
		{
			title: "Attn's Email",
			dataIndex: 'attnEmail',
			render: data => (data ? data : 'Informacion no proporcionada')
		},
		{
			title: 'CC',
			dataIndex: 'cc',
			render: data => (data ? data : 'Informacion no proporcionada')
		},
		{
			title: "CC's Email",
			dataIndex: 'ccEmail',
			render: data => (data ? data : 'Informacion no proporcionada')
		},
		{
			title: 'Actions',
			// eslint-disable-next-line react/display-name
			render: data => <RowButtons data={data} />
		}
	];

	const RowButtons = ({ data }) => {
		return (
			<Row type='flex' justify='space-around'>
				<Col span={6}>
					<Link
						to={{
							pathname: `/dashboard/supplier/edit/${data.uuid}`,
							state: data
						}}
					>
						<Icon type='edit' />
					</Link>
				</Col>
				<Col span={6}>
					<Popconfirm
						title='¿Estas seguro que deseas eliminar?'
						onConfirm={() => handleDelete(data.uuid)}
					>
						<Icon type='delete' />
					</Popconfirm>
				</Col>
			</Row>
		);
	};

	if (completeForm && completeForm.success) {
		notification.open({
			message: completeForm.success ? 'Success' : 'Error',
			description: completeForm.message,
			onClick: () => {
				console.log('Notification Clicked!');
			}
		});
		setState({
			...state,
			data: [...suppliers],
			pagination: {
				...pagination,
				defaultPageSize: 15,
				pageSize: 15
			},
			loading: false
		});
		dispatch(cleanComplete());
	}

	const handleDelete = data => {
		setState({
			...state,
			loading: true
		});
		dispatch(deleteItem(data, 'suppliers'));
	};

	RowButtons.propTypes = {
		data: PropTypes.object.isRequired
	};

	return (
		<DashboardLayout>
			<Table
				columns={columns}
				rowKey={record => record.uuid}
				dataSource={state.data}
				pagination={state.pagination}
				loading={state.loading}
				onChange={handleTableChange}
			/>
		</DashboardLayout>
	);
};

export default Suppliers;
