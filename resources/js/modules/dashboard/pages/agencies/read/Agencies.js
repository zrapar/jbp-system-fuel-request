import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { Table, Popconfirm, Icon, Row, Col, notification } from 'antd';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getAgenciesData, cleanComplete, deleteItem } from '../../../services';
import DashboardLayout from '../../../layout/DashboardLayout';

const Agencies = () => {
	const dispatch = useDispatch();
	const agencies = useSelector(({ dashboard }) => dashboard.agencies);
	const pagination = useSelector(
		({ dashboard }) => dashboard.pagination.agencies
	);
	const completeForm = useSelector(({ dashboard }) => dashboard.completeForm);
	const [state, setState] = useState({
		data: [],
		pagination: {},
		loading: true
	});

	const initCallback = useCallback(() => {
		if (!pagination) {
			dispatch(getAgenciesData());
		}

		setState({
			...state,
			data: [...agencies],
			pagination: {
				...pagination,
				defaultPageSize: 15,
				pageSize: 15
			},
			loading: false
		});

		if (completeForm && completeForm.success) {
			notification.open({
				message: completeForm.success ? 'Success' : 'Error',
				description: completeForm.message,
				onClick: () => {
					console.log('Notification Clicked!');
				}
			});
			dispatch(cleanComplete());
		}
	}, [completeForm, pagination, agencies]);

	useEffect(() => {
		initCallback();
	}, [pagination, agencies]);

	const handleTableChange = (pagination, filters, sorter) => {
		if (state.pagination.currentPage !== pagination.current) {
			setState({ ...state, loading: true });
			dispatch(getAgenciesData(pagination.current));
		}
		console.log(filters);
		console.log(sorter);
	};

	const columns = [
		{
			title: 'Agency Name',
			dataIndex: 'agency'
		},
		{
			title: "Agency's Email",
			dataIndex: 'agencyEmail'
		},
		{
			title: "Attn's Email",
			dataIndex: 'attnEmail',
			render: data => (data ? data : 'Informacion no proporcionada')
		},
		{
			title: "CC's Email",
			dataIndex: 'ccEmail',
			render: data => (data ? data : 'Informacion no proporcionada')
		},
		{
			title: 'Actions',
			// eslint-disable-next-line react/display-name
			render: data => <RowButtons data={data} />
		}
	];

	const RowButtons = ({ data }) => {
		return (
			<Row type='flex' justify='space-around'>
				<Col span={6}>
					<Link
						to={{
							pathname: `/dashboard/agency/edit/${data.uuid}`,
							state: data
						}}
					>
						<Icon type='edit' />
					</Link>
				</Col>
				<Col span={6}>
					<Popconfirm
						title='¿Estas seguro que deseas eliminar?'
						onConfirm={() => handleDelete(data.uuid)}
					>
						<Icon type='delete' />
					</Popconfirm>
				</Col>
			</Row>
		);
	};

	const handleDelete = data => {
		setState({
			...state,
			loading: true
		});
		dispatch(deleteItem(data, 'agencies'));
	};

	RowButtons.propTypes = {
		data: PropTypes.object.isRequired
	};

	return (
		<DashboardLayout>
			<Table
				columns={columns}
				rowKey={record => record.uuid}
				dataSource={state.data}
				pagination={state.pagination}
				loading={state.loading}
				onChange={handleTableChange}
			/>
		</DashboardLayout>
	);
};

export default Agencies;
