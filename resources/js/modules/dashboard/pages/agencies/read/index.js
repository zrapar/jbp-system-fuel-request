// import libs
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

// import components
import Agencies from './Agencies';

export default withRouter(connect()(Agencies));
