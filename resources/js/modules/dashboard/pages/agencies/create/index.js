// import libs
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

// import components
import createAgency from './createAgency';

export default withRouter(connect()(createAgency));
