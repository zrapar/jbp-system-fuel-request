// import libs
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

// import components
import Clients from './Clients';

export default withRouter(connect()(Clients));
