import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Table, Popconfirm, Icon, Row, Col, notification } from 'antd';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getClientsData, cleanComplete, deleteItem } from '../../../services';
import DashboardLayout from '../../../layout/DashboardLayout';

const Clients = () => {
	const dispatch = useDispatch();
	const clients = useSelector(({ dashboard }) => dashboard.clients);
	const pagination = useSelector(
		({ dashboard }) => dashboard.pagination.clients
	);
	const completeForm = useSelector(({ dashboard }) => dashboard.completeForm);
	const [state, setState] = useState({
		data: [],
		pagination: {},
		loading: true
	});

	useEffect(() => {
		if (pagination) {
			setState({
				...state,
				data: [...clients],
				pagination: {
					...pagination,
					defaultPageSize: 15,
					pageSize: 15
				},
				loading: false
			});
		} else {
			dispatch(getClientsData());
		}
	}, [pagination]);

	const handleTableChange = (pagination, filters, sorter) => {
		if (state.pagination.currentPage !== pagination.current) {
			setState({ ...state, loading: true });
			dispatch(getClientsData(pagination.current));
		}
		console.log(filters);
		console.log(sorter);
	};

	const columns = [
		{
			title: 'Client',
			dataIndex: 'client'
		},
		{
			title: "Client's Email",
			dataIndex: 'clientEmail'
		},

		{
			title: "Attn's Email",
			dataIndex: 'attnEmail',
			render: data => (data ? data : 'Informacion no proporcionada')
		},

		{
			title: "CC's Email",
			dataIndex: 'ccEmail',
			render: data => (data ? data : 'Informacion no proporcionada')
		},
		{
			title: 'Actions',
			// eslint-disable-next-line react/display-name
			render: data => <RowButtons data={data} />
		}
	];

	const RowButtons = ({ data }) => {
		return (
			<Row type='flex' justify='space-around'>
				<Col span={6}>
					<Link
						to={{
							pathname: `/dashboard/client/edit/${data.uuid}`,
							state: data
						}}
					>
						<Icon type='edit' />
					</Link>
				</Col>
				<Col span={6}>
					<Popconfirm
						title='¿Estas seguro que deseas eliminar?'
						onConfirm={() => handleDelete(data.uuid)}
					>
						<Icon type='delete' />
					</Popconfirm>
				</Col>
			</Row>
		);
	};

	if (completeForm && completeForm.success) {
		notification.open({
			message: completeForm.success ? 'Success' : 'Error',
			description: completeForm.message,
			onClick: () => {
				console.log('Notification Clicked!');
			}
		});
		setState({
			...state,
			data: [...clients],
			pagination: {
				...pagination,
				defaultPageSize: 15,
				pageSize: 15
			},
			loading: false
		});
		dispatch(cleanComplete());
	}

	const handleDelete = data => {
		setState({
			...state,
			loading: true
		});
		dispatch(deleteItem(data, 'clients'));
	};

	RowButtons.propTypes = {
		data: PropTypes.object.isRequired
	};

	return (
		<DashboardLayout>
			<Table
				columns={columns}
				rowKey={record => record.uuid}
				dataSource={state.data}
				pagination={state.pagination}
				loading={state.loading}
				onChange={handleTableChange}
			/>
		</DashboardLayout>
	);
};

export default Clients;
