import React from 'react';
import PropTypes from 'prop-types';
import DashboardLayout from '../../../layout/DashboardLayout';
import useForm from '../../../../../utils/useForm';
import { Input, Button, Col, Row } from 'antd';
import { saveClients, cleanError } from '../../../services';
import { useDispatch, useSelector } from 'react-redux';
import _ from 'lodash';

const InputGroup = Input.Group;

const initialForm = {
	client: '',
	clientEmail: '',
	attnEmail: '',
	ccEmail: ''
};

const createClient = ({ history }) => {
	const dispatch = useDispatch();

	const completeForm = useSelector(({ dashboard }) => dashboard.completeForm);

	const isEdited =
		_.has(history.location, 'state') &&
		history.location.pathname.search('edit') !== -1;

	if (isEdited && _.has(history.location, 'state') && !history.location.state) {
		history.push({
			pathname: '/dashboard/clients'
		});
	}

	if (completeForm) {
		if (completeForm.success) {
			history.push({
				pathname: '/dashboard/clients'
			});
		} else {
			Object.keys(completeForm.errors).map(it => {
				const element = document.getElementsByName(it)[0];
				const parent = element.parentElement;
				parent.classList.add('has-error');
				completeForm.errors[it].map(i => {
					let newDiv = document.createElement('div');
					newDiv.classList.add('ant-form-explain');
					const textError = document.createTextNode(i);
					newDiv.appendChild(textError);
					parent.appendChild(newDiv);
				});
			});
		}
	}

	const formState =
		_.has(history.location, 'state') && history.location.state
			? history.location.state
			: initialForm;

	const { form, handleChange } = useForm(formState);

	const { client, clientEmail, attnEmail, ccEmail } = form;

	const changeForm = ev => {
    if(ev.target.type !== 'checkbox') {
      ev.target.value = event.target.value.toUpperCase();
    }
		handleChange(ev);
		if (completeForm && !completeForm.success) {
			const element = document.getElementsByName(ev.target.name)[0];
			const parent = element.parentElement;
			parent.classList.remove('has-error');
			[...document.getElementsByClassName('ant-form-explain')].map(a =>
				a.remove()
			);
			dispatch(cleanError(ev.target.name));
		}
	};

	const submitForm = ev => {
		ev.preventDefault();
		dispatch(saveClients(form, isEdited));
	};
	const validateEmail = email => {
		// eslint-disable-next-line no-useless-escape
		const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email.toLowerCase());
	};
	const validForm = () => {
		return (
			client.length > 0 && clientEmail.length > 0 && validateEmail(clientEmail)
		);
	};

	return (
		<DashboardLayout>
			<Row type='flex' justify='center'>
				<Col span={12}>{isEdited ? 'Save Client' : 'Create new client'}</Col>
			</Row>
			<br />
			<form onSubmit={submitForm}>
				<InputGroup size='large'>
					<Row type='flex' justify='center'>
						<Col span={12}>
							<Input
								value={client}
								name='client'
								onChange={changeForm}
								placeholder={`Client's Name`}
							/>
						</Col>
					</Row>
					<br />
					<Row type='flex' justify='center'>
						<Col span={12}>
							<Input
								value={clientEmail}
								name='clientEmail'
								onChange={changeForm}
								placeholder={`Clients's Email`}
							/>
						</Col>
					</Row>
					<br />
					<Row type='flex' justify='center'>
						<Col span={12}>
							<Input
								value={attnEmail}
								name='attnEmail'
								onChange={changeForm}
								placeholder={`Attn's Email`}
							/>
						</Col>
					</Row>
					<br />
					<Row type='flex' justify='center'>
						<Col span={12}>
							<Input
								value={ccEmail}
								name='ccEmail'
								onChange={changeForm}
								placeholder={`CC's Email`}
							/>
						</Col>
					</Row>
					<br />

					<Row type='flex' justify='center'>
						<Col span={12}>
							<Button
								type='primary'
								onClick={submitForm}
								disabled={!validForm()}
							>
								{isEdited ? 'Save Client' : 'Create Client'}
							</Button>
						</Col>
					</Row>
				</InputGroup>
			</form>
		</DashboardLayout>
	);
};

createClient.propTypes = { history: PropTypes.object.isRequired };

export default createClient;
