// import libs
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

// import components
import createClient from './createClient';

export default withRouter(connect()(createClient));
