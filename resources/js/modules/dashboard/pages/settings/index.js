// import libs
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

// import components
import Settings from './Settings';

export default withRouter(connect()(Settings));
