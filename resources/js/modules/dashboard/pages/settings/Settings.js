import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Input, Button, Col, Row, Form, Checkbox } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import DashboardLayout from '../../layout/DashboardLayout';
import { updateSelf } from '../../services';

const formItemLayout = {
	labelCol: {
		xs: { span: 24 },
		sm: { span: 8 }
	},
	wrapperCol: {
		xs: { span: 24 },
		sm: { span: 16 }
	}
};
const tailFormItemLayout = {
	wrapperCol: {
		xs: {
			span: 24,
			offset: 0
		},
		sm: {
			span: 16,
			offset: 8
		}
	}
};

const Settings = ({ history, form }) => {
	const dispatch = useDispatch();

	const user = useSelector(({ user }) => user);

	const [changePass, toggleChangePass] = useState(false);

	const { getFieldDecorator } = form;

	const onSumbit = e => {
		e.preventDefault();
		form.validateFields((err, values) => {
			if (!err) {
				dispatch(updateSelf(values, user.id));
			}
		});
	};

	const handleChange = e => {
		toggleChangePass(e.target.checked);
	};

	return (
		<DashboardLayout>
			<Row type='flex' justify='center'>
				<Col span={12}>Settings</Col>
			</Row>
			<br />
			<Form {...formItemLayout} onSubmit={onSumbit}>
				<Form.Item
					{...formItemLayout}
					hasFeedback={true}
					label='Email to Login'
				>
					{getFieldDecorator('email', {
						initialValue: user.email,
						rules: [
							{
								required: true,
								message: 'This value is required'
							},
							{
								type: 'email',
								message: 'Please input a valid email'
							}
						]
					})(<Input placeholder='Please input email to login' />)}
				</Form.Item>
				<Form.Item
					{...formItemLayout}
					hasFeedback={true}
					label='Email to recieve fuel requests'
				>
					{getFieldDecorator('emailToSend', {
						initialValue: user.emailToSend,
						rules: [
							{
								required: true,
								message: 'Please input your name'
							},
							{
								type: 'email',
								message: 'Please input a valid email'
							}
						]
					})(
						<Input placeholder='Please input the email to recieve fuel requests' />
					)}
				</Form.Item>

				{changePass && (
					<React.Fragment>
						<Form.Item {...formItemLayout} label='Password' hasFeedback>
							{getFieldDecorator('password', {
								initialValue: '',
								rules: [
									{
										required: changePass,
										message: 'Please input your password!'
									}
								]
							})(<Input.Password />)}
						</Form.Item>
						<Form.Item {...formItemLayout} label='Confirm Password' hasFeedback>
							{getFieldDecorator('confirm', {
								initialValue: '',
								rules: [
									{
										required: changePass,
										message: 'Please confirm your password!'
									},
									{
										validator: (rule, value) =>
											value === form.getFieldValue('password'),
										message: 'Passwords mismatch'
									}
								]
							})(<Input.Password />)}
						</Form.Item>
					</React.Fragment>
				)}
				<Form.Item {...tailFormItemLayout}>
					<Checkbox checked={changePass} onChange={handleChange}>
						Change password
					</Checkbox>
				</Form.Item>
				<Form.Item {...tailFormItemLayout}>
					<Button type='primary' htmlType='submit'>
						Register
					</Button>
				</Form.Item>
			</Form>
		</DashboardLayout>
	);
};

Settings.propTypes = { history: PropTypes.object.isRequired };

export default Form.create()(Settings);
