import Http from '../../utils/Http';
import Transformer from '../../utils/Transformer';
import * as dashboardActions from './store/actions';
// import _ from 'lodash';

export const getRequestsData = (item = false) => async dispatch => {
	let url = item ? `fuel?page=${item}` : 'fuel';

	const { data } = await Http.get(url);
	const response = Transformer.fetch(data);
	if (response.success) {
		dispatch(dashboardActions.getRequests(response.data));
	}
};

export const getSuppliersData = (item = false) => async dispatch => {
	let url = item ? `suppliers?page=${item}` : 'suppliers';
	const { data } = await Http.get(url);
	const response = Transformer.fetch(data);

	dispatch(dashboardActions.getSuppliers(response.data));
};

export const saveSupplier = (formData, isEdited) => async dispatch => {
	try {
		const { data } = isEdited
			? await Http.put(`suppliers/update/${formData.uuid}`, formData)
			: await Http.post('suppliers/create', formData);
		const response = Transformer.fetch(data);
		response.type = isEdited ? 'update' : 'new';
		return dispatch(dashboardActions.saveSupplierApi(response));
	} catch ({ response, request }) {
		if (response && response.data) {
			return dispatch(
				dashboardActions.errorMessage({
					success: false,
					message: 'Existen errores',
					errors: response.data.errors
				})
			);
		}

		if (request && request.status !== 0) {
			return dispatch(
				dashboardActions.errorMessage({
					success: false,
					message: 'Ha ocurrido un error, intente de nuevo'
				})
			);
		}
		return dispatch(
			dashboardActions.errorMessage({
				success: false,
				message: 'Revise su conexion a internet'
			})
		);
	}
};

export const getClientsData = (item = false) => async dispatch => {
	let url = item ? `clients?page=${item}` : 'clients';
	const { data } = await Http.get(url);
	const response = Transformer.fetch(data);
	dispatch(dashboardActions.getClients(response.data));
};

export const saveClients = (formData, isEdited) => async dispatch => {
	try {
		const { data } = isEdited
			? await Http.put(`clients/update/${formData.uuid}`, formData)
			: await Http.post('clients/create', formData);
		const response = Transformer.fetch(data);
		response.type = isEdited ? 'update' : 'new';
		dispatch(dashboardActions.saveClientApi(response));
	} catch ({ response, request }) {
		if (response && response.data) {
			return dispatch(
				dashboardActions.errorMessage({
					success: false,
					message: 'Existen errores',
					errors: response.data.errors
				})
			);
		}

		if (request && request.status !== 0) {
			return dispatch(
				dashboardActions.errorMessage({
					success: false,
					message: 'Ha ocurrido un error, intente de nuevo'
				})
			);
		}
		return dispatch(
			dashboardActions.errorMessage({
				success: false,
				message: 'Revise su conexion a internet'
			})
		);
	}
};

export const getAgenciesData = (item = false) => async dispatch => {
	let url = item ? `agencies?page=${item}` : 'agencies';
	const { data } = await Http.get(url);
	const response = Transformer.fetch(data);
	dispatch(dashboardActions.getAgencies(response.data));
};

export const saveAgency = (formData, isEdited) => async dispatch => {
	try {
		const { data } = isEdited
			? await Http.put(`agencies/update/${formData.uuid}`, formData)
			: await Http.post('agencies/create', formData);
		const response = Transformer.fetch(data);
		response.type = isEdited ? 'update' : 'new';
		dispatch(dashboardActions.saveAgencyApi(response));
	} catch ({ response, request }) {
		if (response && response.data) {
			return dispatch(
				dashboardActions.errorMessage({
					success: false,
					message: 'Existen errores',
					errors: response.data.errors
				})
			);
		}

		if (request && request.status !== 0) {
			return dispatch(
				dashboardActions.errorMessage({
					success: false,
					message: 'Ha ocurrido un error, intente de nuevo'
				})
			);
		}
		return dispatch(
			dashboardActions.errorMessage({
				success: false,
				message: 'Revise su conexion a internet'
			})
		);
	}
};

export const deleteItem = (uuid, resource) => async dispatch => {
	const { data } = await Http.delete(`${resource}/delete/${uuid}`);
	const response = Transformer.fetch(data);
	dispatch(dashboardActions.deleteItem(response, resource, uuid));
};

export const cleanComplete = () => async dispatch => {
	dispatch(dashboardActions.cleanComplete());
};
export const cleanError = name => async dispatch => {
	dispatch(dashboardActions.cleanError(name));
};

export const updateSelf = (formData, id) => async dispatch => {
	try {
		const { data } = await Http.put(`users/${id}`, Transformer.send(formData));
		const { user, message, success } = Transformer.fetch(data);

		dispatch(dashboardActions.updateSelf(user));
		dispatch(dashboardActions.updateMessage({ message, success }));
	} catch ({ response, request }) {
		if (response && response.data) {
			return dispatch(
				dashboardActions.errorMessage({
					success: false,
					message: 'Existen errores',
					errors: response.data.errors
				})
			);
		}

		if (request && request.status !== 0) {
			return dispatch(
				dashboardActions.errorMessage({
					success: false,
					message: 'Ha ocurrido un error, intente de nuevo'
				})
			);
		}
		return dispatch(
			dashboardActions.errorMessage({
				success: false,
				message: 'Revise su conexion a internet'
			})
		);
	}
};

export const sendMailService = uuid => async dispatch => {
	try {
		const { data } = await Http.patch(`fuel/send-mail/${uuid}`);
		const { message, success } = Transformer.fetch(data);

		if (success) {
			return dispatch(getRequestsData());
		}
		return dispatch(
			dashboardActions.errorMessage({
				success: false,
				message: 'Ha Ocurrido un error',
				errors: message
			})
		);
	} catch ({ response, request }) {
		if (response && response.data) {
			return dispatch(
				dashboardActions.errorMessage({
					success: false,
					message: 'Existen errores',
					errors: response.data.errors
				})
			);
		}

		if (request && request.status !== 0) {
			return dispatch(
				dashboardActions.errorMessage({
					success: false,
					message: 'Ha ocurrido un error, intente de nuevo'
				})
			);
		}
		return dispatch(
			dashboardActions.errorMessage({
				success: false,
				message: 'Revise su conexion a internet'
			})
		);
	}
};
