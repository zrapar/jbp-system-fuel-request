// import HTTP from '../../../utils/Http';
import {
	FORM_SHOW_MESSAGE,
	FORM_CLOSE_MESSAGE,
	SET_AIRPORTS,
	SET_RESPONSE_DATA,
	SENT_COMPLETE_FORM,
	SET_AIRCRAFTS,
	SHOW_DELETE_MESSAGE,
	CLOSE_DELETE_MESSAGE,
	SET_SUPPLIERS,
	SET_CLIENTS,
	SET_AGENCIES,
	SET_LAST_INVOICE
} from './action-types';
// import { showMessage } from './actions';

const initialState = {
	message: null,
	airports: [],
	aircrafts: [],
	suppliers: [],
	clients: [],
	agencies: [],
	lastInvo: null,
	response: null,
	completeForm: null,
	deleteMessage: null
};

const reducer = (state = initialState, { type, payload = null }) => {
	switch (type) {
		case FORM_SHOW_MESSAGE:
			return showMessage(state, payload);
		case FORM_CLOSE_MESSAGE:
			return closeMessage(state);
		case SET_AIRPORTS:
			return setAirport(state, payload);
		case SET_AIRCRAFTS:
			return setAircraft(state, payload);
		case SET_SUPPLIERS:
			return setSupplier(state, payload);
		case SET_CLIENTS:
			return setClient(state, payload);
		case SET_AGENCIES:
			return setAgency(state, payload);
		case SET_RESPONSE_DATA:
			return responseData(state, payload);
		case SET_LAST_INVOICE:
			return lastInvoice(state, payload);
		case SENT_COMPLETE_FORM:
			return completeForm(state, payload);
		case SHOW_DELETE_MESSAGE:
			return deleteMessage(state, payload);
		case CLOSE_DELETE_MESSAGE:
			return closeDeleteMessage(state);
		default:
			return state;
	}
};

const showMessage = (state, payload) => {
	return {
		...state,
		message: {
			success: payload.success,
			message: payload.message
		}
	};
};
const deleteMessage = (state, payload) => {
	return {
		...state,
		deleteMessage: {
			success: payload.success,
			message: payload.message
		}
	};
};
const closeDeleteMessage = state => {
	return {
		...state,
		deleteMessage: null,
		completeForm: null
	};
};
const setAirport = (state, payload) => {
	return {
		...state,
		airports: [...state.airports, ...payload]
	};
};
const lastInvoice = (state, payload) => {
	return {
		...state,
		lastInvo: payload
	};
};
const setAircraft = (state, payload) => {
	return {
		...state,
		aircrafts: [...state.aircrafts, ...payload]
	};
};
const responseData = (state, payload) => {
	return {
		...state,
		response: payload
	};
};
const closeMessage = state => {
	return {
		...state,
		message: null
	};
};

const completeForm = (state, payload) => {
	return {
		...state,
		completeForm: payload
	};
};

const setSupplier = (state, payload) => {
	return {
		...state,
		suppliers: [...payload]
	};
};
const setClient = (state, payload) => {
	return {
		...state,
		clients: [...payload]
	};
};
const setAgency = (state, payload) => {
	return {
		...state,
		agencies: [...payload]
	};
};

export default reducer;
