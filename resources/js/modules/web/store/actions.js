/* ============
 * Actions for the auth module
 * ============
 *
 * The actions that are available on the
 * auth module.
 */

import {
	FORM_SHOW_MESSAGE,
	FORM_CLOSE_MESSAGE,
	SET_AIRPORTS,
	SET_AIRCRAFTS,
	SET_RESPONSE_DATA,
	SENT_COMPLETE_FORM,
	SHOW_DELETE_MESSAGE,
	CLOSE_DELETE_MESSAGE,
	SET_SUPPLIERS,
	SET_CLIENTS,
	SET_AGENCIES,
	SET_LAST_INVOICE
} from './action-types';

export const showMessage = ({ success, message }) => {
	return { type: FORM_SHOW_MESSAGE, payload: { success, message } };
};
export const closeMessage = () => {
	return { type: FORM_CLOSE_MESSAGE };
};
export const setAirports = ({ data }) => {
	return { type: SET_AIRPORTS, payload: data };
};
export const setSuppliers = ({ data }) => {
	return { type: SET_SUPPLIERS, payload: data };
};
export const setClients = ({ data }) => {
	return { type: SET_CLIENTS, payload: data };
};
export const setAgencies = ({ data }) => {
	return { type: SET_AGENCIES, payload: data };
};
export const setAircrafts = ({ data }) => {
	return { type: SET_AIRCRAFTS, payload: data };
};
export const getResponseData = ({ data }) => {
	return { type: SET_RESPONSE_DATA, payload: data };
};
export const lastInvoice = data => {
	return { type: SET_LAST_INVOICE, payload: data };
};
export const showMessageComplete = ({ success, message }) => {
	return { type: SENT_COMPLETE_FORM, payload: { success, message } };
};
export const showMessageDelete = ({ success, message }) => {
	return { type: SHOW_DELETE_MESSAGE, payload: { success, message } };
};
export const closeMessageDeleteAction = () => {
	return { type: CLOSE_DELETE_MESSAGE };
};
