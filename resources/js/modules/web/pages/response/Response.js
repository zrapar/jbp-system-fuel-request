import React, { useState, useEffect, Fragment } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import Select from 'react-select';
import { useSelector } from 'react-redux';
import {
	getDataResponse,
	sendCompleteForm,
	deleteFuelRequest,
	getSuppliers,
	getClients,
	getAgencies
} from '../../service';
import Moment from 'react-moment';
import { notification, Row, Col, Spin } from 'antd';

const key = 'Response';

const openNotification = (title, message, type) => {
	notification[type]({
		key,
		message     : title,
		description : message,
		onClick     : () => {
			notification.close(key);
		}
	});
};

const Response = ({ match, dispatch, history }) => {
	if (!match.params.uuid) {
		return <Redirect to='/' />;
	}

	const uuid = match.params.uuid;

	useEffect(() => {
		dispatch(getDataResponse(uuid));
		dispatch(getSuppliers());
		dispatch(getClients());
		dispatch(getAgencies());
	}, []);

	const response = useSelector(({ fuel_request }) => fuel_request.response);
	const suppliers = useSelector(({ fuel_request }) => fuel_request.suppliers);
	const clients = useSelector(({ fuel_request }) => fuel_request.clients);
	const agencies = useSelector(({ fuel_request }) => fuel_request.agencies);
	const lastInvo = useSelector(({ fuel_request }) => fuel_request.lastInvo);
	const complete = useSelector(({ fuel_request }) => fuel_request.completeForm);

	const [ loading, setLoading ] = useState(false);
	const [ typeSend, setType ] = useState(null);

	const customStyles = {
		indicatorSeparator : () => ({ width: '0%' }),
		menu               : (styleBefore) => ({ ...styleBefore, borderRadius: '10px' }),
		valueContainer     : (styleBefore) => ({ ...styleBefore, padding: '0px 0px' }),
		dropdownIndicator  : (styleBefore) => ({
			...styleBefore,
			padding : '0px',
			color   : '#999'
		}),
		control            : (styleBefore) => ({
			...styleBefore,
			backgroundColor : 'transparent !important',
			border          : 'none !important'
		}),
		singleValue        : (styleBefore) => ({
			...styleBefore,
			color      : '#555555',
			fontFamily : 'Montserrat-SemiBold',
			fontSize   : '18px',
			lineHeight : 1.2
		}),
		option             : (styleBefore, { isFocused }) => ({
			...styleBefore,
			fontFamily      : 'Montserrat-SemiBold',
			fontSize        : '14px',
			lineHeight      : 1.2,
			backgroundColor : isFocused ? '#212843' : 'transparent',
			color           : isFocused ? '#fff' : '#555555'
		}),
		placeholder        : (styleBefore) => ({
			...styleBefore,
			marginLeft  : '0px',
			marginRight : '0px',
			color       : '#999',
			fontFamily  : 'Montserrat-SemiBold',
			fontSize    : '18px',
			lineHeight  : 1.2
		})
	};

	const [ formData, setFormData ] = useState({
		supplier    : '',
		customer    : '',
		from        : '',
		uuidRequest : uuid,
		correlative : ''
	});

	useEffect(
		() => {
			if (lastInvo) {
				setFormData({ ...formData, correlative: lastInvo.idInvoice });
			}
		},
		[ lastInvo ]
	);

	const submitForm = (e) => {
		e.preventDefault();
		if (response.isCompleted === '0') {
			if (formData.supplier.length === 0 || formData.customer.length === 0 || formData.from.length === 0) {
				openNotification('Error', 'You dont complete the form', 'error');
			} else {
				setLoading(!loading);
				dispatch(sendCompleteForm({ ...formData, typeSend }));
				openNotification(
					'Success',
					'Please wait meanwhile its generating the confirmation of request',
					'success'
				);
			}
		} else {
			history.push({
				pathname : `/dashboard`,
				state    : response
			});
		}
	};

	const handleChange = (e, name) => {
		setFormData({ ...formData, [name]: e.value });
	};

	const onChange = (e) => {
		setFormData({ ...formData, [e.target.name]: e.target.value });
	};

	const editRequest = () => {
		history.push({
			pathname : `/edit/${uuid}`,
			state    : response
		});
	};

	const deleteRequest = (ev, uuid) => {
		ev.preventDefault();
		dispatch(deleteFuelRequest(uuid));
		setTimeout(() => {
			history.push({
				pathname : `/dashboard`,
				state    : response
			});
		}, 800);
	};

	if (
		complete &&
		// eslint-disable-next-line no-prototype-builtins
		complete.hasOwnProperty('success') &&
		complete.success
	) {
		history.push({
			pathname : '/dashboard'
		});
	}

	return (
		<Fragment>
			{response ? (
				<Spin spinning={loading}>
					<div className='container-contact100'>
						<div className='wrap-contact100' style={{ padding: '62px 55px 220px 55px' }}>
							<form className='contact100-form validate-form' onSubmit={(e) => submitForm(e)}>
								<span className='contact100-form-title'>
									<img src='images/logo.png' alt='Logo JBP' />
									<br />
									REQUEST - Fuel Release
								</span>
								<div className='solicitado'>
									<div className='datoscliente2'>
										<h2>PROVIDED DATA REQUEST / Datos de la Solicitud:</h2>
									</div>
									<Row style={{ padding: '1.5%' }}>
										<Col span={8} className='datoscliente'>
											<p>
												CUSTOMER / Cliente: <br />
												<b>{response['customer']}</b>
											</p>
										</Col>
										<Col span={8} className='datoscliente'>
											<p>
												AC REGISTRATION / Matrícula: <br />
												<b>{response['acRegistration']}</b>
											</p>
										</Col>
										<Col span={8} className='datoscliente'>
											<p>
												AIRCRAFT TYPE / Tipo: <br />
												<b>{response['aircraft']['type']}</b>
											</p>
										</Col>
									</Row>
									<Row style={{ padding: '1.5%' }}>
										<Col span={8} className='datoscliente'>
											<p>
												LOCATION / Localidad: <br />
												<b>{response['departure']['label']}</b>
											</p>
										</Col>
										<Col span={8} className='datoscliente'>
											<p>
												DESTINATION / Destino: <br />
												<b>{response['arrival']['label']}</b>
											</p>
										</Col>
										<Col span={8} className='datoscliente'>
											<p>
												ETA:
												<b>
													<Moment date={response['arrivalDate']} format=' DD / MM / YYYY ' />
												</b>
												Time: {response['arrivalTime']}
												<br />
												ETD:
												<b>
													<Moment
														date={response['departureDate']}
														format=' DD / MM / YYYY '
													/>
												</b>
												Time: {response['departureTime']}
											</p>
										</Col>
									</Row>
									<Row style={{ padding: '1.5%' }}>
										<Col span={8} className='datoscliente'>
											<p>
												PRODUCT/ Producto: <br />
												<b>{response['typeProduct']}</b>
											</p>
										</Col>
										<Col span={8} className='datoscliente'>
											<p>
												FLIGHT TYPE / Tipo de Vuelo: <br />
												<b>{response['flightType']}</b>
											</p>
										</Col>
										<Col span={8} className='datoscliente'>
											<p>
												OPERATION TYPE / Tipo de Vuelo: <br />
												<b>
													{response['flightOperation'] === 'charter' ? (
														'AOC/Charter/Corp for Hire'
													) : (
														'Corporative/Private'
													)}
												</b>
											</p>
										</Col>
									</Row>
									<Row style={{ padding: '1.5%' }}>
										{response['handler'] && (
											<Col span={8} className='datoscliente'>
												<p>
													HANDLER: <br />
													<b>{response['handler']}</b>
												</p>
											</Col>
										)}

										<Col span={8} className='datoscliente'>
											<p>
												UPLIFT / Carga: <br />
												{response['gallons'] === "At Captain's request" ? (
													<b>{response['gallons']}</b>
												) : (
													<b>{response['gallons']} Galons </b>
												)}
											</p>
										</Col>
									</Row>

									{response['remarks'] && (
										<div className='datoscliente2'>
											<p>
												REMARKS/ Observaciones:{'  '}
												<b>{response['remarks']}</b>
											</p>
										</div>
									)}
								</div>
								{response.isCompleted !== '1' ? (
									<React.Fragment>
										<div className='datosbutt'>
											<button className='bluedit' onClick={(ev) => editRequest(ev)}>
												<i className='fa fa-pencil' aria-hidden='true' /> Editar Datos
											</button>
											<button
												className='rederase'
												onClick={(ev) => deleteRequest(ev, response['uuid'])}
											>
												<i className='fa fa-times-circle' aria-hidden='true' /> Eliminar
												Solicitud
											</button>
											<button
												className='rederase'
												onClick={(ev) => {
													ev.preventDefault();
													history.push({ pathname: `/dashboard` });
												}}
											>
												<i className='fa fa-arrow-left' aria-hidden='true' /> Volver al
												Dashboard
											</button>
										</div>
										<div className='datosbutt'>
											<button
												className='bluedit'
												onClick={(ev) => {
													ev.preventDefault();
													history.push({ pathname: `/dashboard/supplier/create` });
												}}
											>
												<i className='fa fa-floppy-o' aria-hidden='true' /> CREATE SUPPLIER /
												CREAR PROVEEDOR:
											</button>
											<button
												className='bluedit'
												onClick={(ev) => {
													ev.preventDefault();
													history.push({ pathname: `/dashboard/client/create` });
												}}
											>
												<i className='fa fa-floppy-o' aria-hidden='true' /> CREATE CUSTOMER /
												CREAR CLIENTE
											</button>
										</div>

										<div className='wrap-input100 input100-select bg1 rs1-wrap-input100'>
											<span className='label-input100'>SUPPLIER / PROVEEDOR: *</span>
											<div>
												<Select
													classNamePrefix='select'
													placeholder='Seleccione uno...'
													name='supplier'
													styles={customStyles}
													options={suppliers}
													onChange={(e) => handleChange(e, 'supplier')}
													isSearchable={false}
												/>
											</div>
										</div>
										<div className='wrap-input100 validate-input bg1 rs1-wrap-input100'>
											<span className='label-input100'>CUSTOMER / Cliente *</span>
											<div>
												<Select
													classNamePrefix='select'
													placeholder='Seleccione uno...'
													name='customer'
													styles={customStyles}
													options={clients}
													onChange={(e) => handleChange(e, 'customer')}
													isSearchable={false}
												/>
											</div>
										</div>

										<div className='wrap-input100 validate-input bg1 rs1-wrap-input100'>
											<span className='label-input100'>FROM / De: *</span>
											<div>
												<Select
													classNamePrefix='select'
													placeholder='Escoga uno...'
													name='from'
													styles={customStyles}
													options={agencies}
													onChange={(e) => handleChange(e, 'from')}
													isSearchable={false}
												/>
											</div>
										</div>
										<div className='wrap-input100 validate-input bg1 rs1-wrap-input100'>
											<span className='label-input100'>Correlativo</span>
											<input
												className='input100'
												onChange={(e) => onChange(e)}
												value={formData.correlative}
												name='correlative'
												placeholder='Correlativo...'
											/>
										</div>

										<div className='rs1-wrap-input100'>
											<button
												className='contact100-form-btn'
												type='submit'
												onClick={() => setType(true)}
											>
												<span>
													CONFIRMAR Y ENVIAR
													<i className='fa fa-paper-plane-o m-l-7' aria-hidden='true' />
												</span>
											</button>
										</div>
										<div className='rs1-wrap-input100'>
											<button
												style={{ backgroundColor: '#488356' }}
												className='contact100-form-btn'
												type='submit'
												onClick={() => setType(false)}
											>
												<span>
													SOLO CONFIRMAR
													<i className='fa fa-floppy-o m-l-7' aria-hidden='true' />
												</span>
											</button>
										</div>
									</React.Fragment>
								) : (
									<React.Fragment>
										<div className='datosbutt'>
											<button
												className='rederase'
												onClick={(ev) => {
													ev.preventDefault();
													history.push({ pathname: `/dashboard` });
												}}
											>
												<i className='fa fa-arrow-left' aria-hidden='true' /> Volver al
												Dashboard
											</button>
										</div>
										<div className='container-contact100-form-btn'>
											<button className='contact100-form-btn' type='submit'>
												<span>
													ESTA ORDEN YA FUE CONFIRMADA
													<i className='fa fa-long-arrow-right m-l-7' aria-hidden='true' />
												</span>
											</button>
										</div>
									</React.Fragment>
								)}
							</form>
						</div>
					</div>
				</Spin>
			) : null}
		</Fragment>
	);
};

Response.propTypes = {
	match    : PropTypes.object.isRequired,
	history  : PropTypes.object.isRequired,
	dispatch : PropTypes.func.isRequired
};

export default Response;
