// import libs
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

// import components
import Response from './Response';

export default withRouter(connect()(Response));
