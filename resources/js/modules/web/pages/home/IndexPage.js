/* eslint-disable no-useless-escape */
import React, { useState, useEffect, Fragment, useRef } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import Creatable from 'react-select/creatable';
import AsyncSelect from 'react-select/async';
import {
	sendForm,
	// getAirports,
	searchAirport,
	getAircraft,
	closeMessageDelete
} from '../../service';
import { useSelector } from 'react-redux';
import { notification, Modal } from 'antd';
import moment from 'moment';

const key = 'IndexPage';

const IndexPage = ({ dispatch }) => {
	const initialForm = {
		customer: '',
		email: '',
		acRegistration: '',
		aircraftType: '',
		departure: '',
		arrival: '',
		departureDate: '',
		arrivalDate: '',
		typeProduct: 'Jet A1',
		gallons: '',
		remarks: '',
		flightOperation: 'corporative',
		flightType: 'international',
		departureTime: '',
		arrivalTime: '',
		captainRequest: '',
		handler: ''
	};

	const [formData, setFormData] = useState(initialForm);

	const initialModal = {
		visible: false,
		confirmLoading: false,
		text: 'Comfirm data before send it'
	};
	const [modalData, setModalData] = useState(initialModal);

	const messageSuccess = useSelector(
		({ fuel_request }) => fuel_request.message
	);
	const aircrafts = useSelector(({ fuel_request }) => fuel_request.aircrafts);

	const refAircraft = useRef(null);
	const refDeparture = useRef(null);
	const refArrival = useRef(null);
	const refTimeD = useRef(null);
	const refTimeA = useRef(null);

	useEffect(() => {
		// dispatch(getAirports());
		dispatch(getAircraft());
		dispatch(closeMessageDelete());
		if (messageSuccess && messageSuccess.success) {
			setModalData(initialModal);
			setFormData(initialForm);
			refAircraft.current.state.value = null;
			refDeparture.current.select.state.value = null;
			refArrival.current.select.state.value = null;
			refTimeD.current.state.value = null;
			refTimeA.current.state.value = null;
			notification['success']({
				key,
				message: 'Success',
				description: 'Your request its succesfully agended',
				onClick: () => {
					notification.close(key);
				}
			});
		}
	}, [messageSuccess]);

	const loadOptions = inputValue =>
		new Promise(resolve => {
			setTimeout(() => {
				resolve(searchAirport(inputValue));
			}, 1000);
		});

	const times = [
		{ label: '00:00', value: '00:00' },
		{ label: '00:30', value: '00:30' },
		{ label: '01:00', value: '01:00' },
		{ label: '01:30', value: '01:30' },
		{ label: '02:00', value: '02:00' },
		{ label: '02:30', value: '02:30' },
		{ label: '03:00', value: '03:00' },
		{ label: '03:30', value: '03:30' },
		{ label: '04:00', value: '04:00' },
		{ label: '04:30', value: '04:30' },
		{ label: '05:00', value: '05:00' },
		{ label: '05:30', value: '05:30' },
		{ label: '06:00', value: '06:00' },
		{ label: '06:30', value: '06:30' },
		{ label: '07:00', value: '07:00' },
		{ label: '07:30', value: '07:30' },
		{ label: '08:00', value: '08:00' },
		{ label: '08:30', value: '08:30' },
		{ label: '09:00', value: '09:00' },
		{ label: '09:30', value: '09:30' },
		{ label: '10:00', value: '10:00' },
		{ label: '10:30', value: '10:30' },
		{ label: '11:00', value: '11:00' },
		{ label: '11:30', value: '11:30' },
		{ label: '12:00', value: '12:00' },
		{ label: '12:30', value: '12:30' },
		{ label: '13:00', value: '13:00' },
		{ label: '13:30', value: '13:30' },
		{ label: '14:00', value: '14:00' },
		{ label: '14:30', value: '14:30' },
		{ label: '15:00', value: '15:00' },
		{ label: '15:30', value: '15:30' },
		{ label: '16:00', value: '16:00' },
		{ label: '16:30', value: '16:30' },
		{ label: '17:00', value: '17:00' },
		{ label: '17:30', value: '17:30' },
		{ label: '18:00', value: '18:00' },
		{ label: '18:30', value: '18:30' },
		{ label: '19:00', value: '19:00' },
		{ label: '19:30', value: '19:30' },
		{ label: '20:00', value: '20:00' },
		{ label: '20:30', value: '20:30' },
		{ label: '21:00', value: '21:00' },
		{ label: '21:30', value: '21:30' },
		{ label: '22:00', value: '22:00' },
		{ label: '22:30', value: '22:30' },
		{ label: '23:00', value: '23:00' },
		{ label: '23:30', value: '23:30' }
	];

	const customStyles = {
		indicatorSeparator: () => ({ width: '0%' }),
		menu: styleBefore => ({ ...styleBefore, borderRadius: '10px' }),
		valueContainer: styleBefore => ({ ...styleBefore, padding: '0px 0px' }),
		dropdownIndicator: styleBefore => ({
			...styleBefore,
			padding: '0px',
			color: '#999'
		}),
		control: styleBefore => ({
			...styleBefore,
			backgroundColor: 'transparent !important',
			border: 'none !important'
		}),
		singleValue: styleBefore => ({
			...styleBefore,
			color: '#555555',
			fontFamily: 'Montserrat-SemiBold',
			fontSize: '18px',
			lineHeight: 1.2
		}),
		option: (styleBefore, { isFocused }) => ({
			...styleBefore,
			fontFamily: 'Montserrat-SemiBold',
			fontSize: '14px',
			lineHeight: 1.2,
			backgroundColor: isFocused ? '#212843' : 'transparent',
			color: isFocused ? '#fff' : '#555555'
		}),
		placeholder: styleBefore => ({
			...styleBefore,
			marginLeft: '0px',
			marginRight: '0px',
			color: '#999',
			fontFamily: 'Montserrat-SemiBold',
			fontSize: '18px',
			lineHeight: 1.2
		})
	};

	const {
		customer,
		email,
		acRegistration,
		departureDate,
		arrivalDate,
		typeProduct,
		gallons,
		remarks,
		flightOperation,
		flightType,
		captainRequest,
		handler
	} = formData;

	const validForm = form => {
		let isValid = true;
		Object.keys(form).map(item => {
			const field = document.getElementsByName(item)[0];

			if (
				item !== 'email' &&
				item !== 'gallons' &&
				item !== 'captainRequest' &&
				item !== 'remarks' &&
				item !== 'typeProduct' &&
				item !== 'flightOperation' &&
				item !== 'flightType' &&
				item !== 'handler' &&
				form[item].length === 0
			) {
				if (field.parentNode.classList.contains('wrap-input100')) {
					if (!field.parentNode.classList.contains('has-errors-input')) {
						field.parentNode.classList.add('has-errors-input');
						const newSpan = document.createElement('span');
						const textError = document.createTextNode(
							`Este campo no puede estar vacio / this field cant be empty`
						);
						newSpan.appendChild(textError);
						field.parentNode.appendChild(newSpan);
					}
				} else {
					if (
						!field.parentNode.parentNode.parentNode.classList.contains(
							'has-errors-input'
						)
					) {
						field.parentNode.parentNode.parentNode.classList.add(
							'has-errors-input'
						);
						const newSpan = document.createElement('span');
						const textError = document.createTextNode(
							`Este campo no puede estar vacio / this field cant be empty`
						);
						newSpan.appendChild(textError);
						field.parentNode.parentNode.parentNode.appendChild(newSpan);
					}
				}

				isValid = false;
			}

			if (item === 'email' && !validateEmail(form[item])) {
				if (!field.parentNode.classList.contains('has-errors-input')) {
					field.parentNode.classList.add('has-errors-input');
					const newSpan = document.createElement('span');
					const textError = document.createTextNode(
						`Este no es un email valid / this it's not a valid email`
					);
					newSpan.appendChild(textError);
					field.parentNode.appendChild(newSpan);
				}
				isValid = false;
			}

			if (
				item === 'gallons' &&
				form['captainRequest'].length === 0 &&
				form[item].length === 0
			) {
				if (!field.parentNode.classList.contains('has-errors-input')) {
					field.parentNode.classList.add('has-errors-input');
					const newSpan = document.createElement('span');
					const textError = document.createTextNode(
						`Este campo no puede estar vacio / this field cant be empty`
					);
					newSpan.appendChild(textError);
					field.parentNode.appendChild(newSpan);
				}
				isValid = false;
			}

			if (
				item === 'arrivalDate' &&
				form['departureDate'] &&
				form['arrivalDate'] &&
				form['arrivalDate'] > form['departureDate']
			) {
				if (!field.parentNode.classList.contains('has-errors-input')) {
					field.parentNode.classList.add('has-errors-input');
					const newSpan = document.createElement('span');
					const textError = document.createTextNode(
						`La fecha de llegada no puede ser mayor a la fecha de salida`
					);
					newSpan.appendChild(textError);
					field.parentNode.appendChild(newSpan);
				}
				isValid = false;
      }
      
			if (
        item === 'arrivalDate' &&
        form['arrivalDate'] &&
				moment(form['arrivalDate']).format('YYYY-MM-DD') < moment(new Date()).format('YYYY-MM-DD')
			) {
				if (!field.parentNode.classList.contains('has-errors-input')) {
					field.parentNode.classList.add('has-errors-input');
					const newSpan = document.createElement('span');
					const textError = document.createTextNode(
						`La fecha de llegada no puede ser menor a la fecha actual ${moment(new Date()).format('YYYY-MMM-DD')}`
					);
					newSpan.appendChild(textError);
					field.parentNode.appendChild(newSpan);
				}
				isValid = false;
      }
      
			if (
        item === 'departureDate' &&
        form['departureDate'] &&
				moment(form['departureDate']).format('YYYY-MM-DD') < moment(new Date()).format('YYYY-MM-DD')
			) {
				if (!field.parentNode.classList.contains('has-errors-input')) {
					field.parentNode.classList.add('has-errors-input');
					const newSpan = document.createElement('span');
					const textError = document.createTextNode(
						`La fecha de salida no puede ser menor a la fecha actual ${moment(new Date()).format('YYYY-MMM-DD')}`
					);
					newSpan.appendChild(textError);
					field.parentNode.appendChild(newSpan);
				}
				isValid = false;
			}

			if (
				item === 'departureDate' &&
				form['departureDate'] &&
				form['arrivalDate'] &&
				form['arrivalDate'] > form['departureDate']
			) {
				if (!field.parentNode.classList.contains('has-errors-input')) {
					field.parentNode.classList.add('has-errors-input');
					const newSpan = document.createElement('span');
					const textError = document.createTextNode(
						`La fecha de salida no puede ser menor a la fecha de llegada`
					);
					newSpan.appendChild(textError);
					field.parentNode.appendChild(newSpan);
				}
				isValid = false;
			}
		});

		return isValid;
	};

	const submitForm = e => {
		e.preventDefault();
    const isValid = validForm(formData);
    
    if (isValid) {
			setModalData({
				...modalData,
				visible: true
			});
		} else {
			notification['error']({
				key,
				message: 'Error',
				description:
					'Tienes errores en el llenado de los campos / Please check the fields for errors',
				onClick: () => {
					removeAllErrors();
					notification.close(key);
				}
			});
		}
	};
  

	const onChange = e => {
		e.preventDefault();
    if (e.target.name === 'gallons') {
      const value = e.target.value.replace(".", "");
      const re = /^([1-9][0-9]{0,4}|99999)$/;
      const pass = re.test(value);
      
      if (pass) {
        setFormData({ ...formData, [e.target.name]: parseInt(value).toLocaleString().replace(",",".").toString() });
      } else if (e.target.value.length === 0) {
        setFormData({ ...formData, [e.target.name]: '' });
      }
    } else {
      setFormData({ ...formData, [e.target.name]: e.target.value.toUpperCase() });
      const field = document.getElementsByName(e.target.name)[0];
      if (field.parentNode.classList.contains('has-errors-input')) {
        field.parentNode.classList.remove('has-errors-input');
        field.parentNode.removeChild(field.parentNode.lastChild);
      }
    }
    
		
		
	};

	const handleChange = (e, name) => {
		setFormData({ ...formData, [name]: e.value });
		const field = document.getElementsByName(name)[0];
		if (
			field.parentNode.parentNode.parentNode.classList.contains(
				'has-errors-input'
			)
		) {
			field.parentNode.parentNode.parentNode.classList.remove(
				'has-errors-input'
			);
			field.parentNode.parentNode.parentNode.removeChild(
				field.parentNode.parentNode.parentNode.lastChild
			);
		}
	};

	const radioChange = (e, name) => {
		if (name === 'captainRequest') {
			captainRequest.length > 0
				? setFormData({ ...formData, [name]: '' })
				: setFormData({ ...formData, [name]: 'captainRequest' });
			const field = document.getElementsByName('gallons')[0];
			if (field.parentNode.classList.contains('has-errors-input')) {
				field.parentNode.classList.remove('has-errors-input');
				field.parentNode.removeChild(field.parentNode.lastChild);
			}
		} else {
			setFormData({ ...formData, [name]: e.target.value });
		}
	};

	const handleOk = () => {
		dispatch(sendForm(formData));
		setModalData({
			...modalData,
			confirmLoading: true,
			text: 'Your request its sending...'
		});
	};

	const validateEmail = email => {
		// eslint-disable-next-line no-useless-escape
		const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email.toLowerCase());
	};

	const handleCancel = () => {
		setModalData(initialModal);
	};

	const removeAllErrors = () => {
		Object.keys(formData).map(item => {
			const field = document.getElementsByName(item)[0];
			if (
				item !== 'captainRequest' &&
				item !== 'remarks' &&
				item !== 'typeProduct' &&
				item !== 'flightOperation' &&
        item !== 'flightType' &&
        item !== 'handler' &&
				formData[item].length === 0
			) {
				if (field.parentNode.classList.contains('wrap-input100')) {
					if (field.parentNode.classList.contains('has-errors-input')) {
						field.parentNode.classList.remove('has-errors-input');
						field.parentNode.removeChild(field.parentNode.lastChild);
					}
				} else {
					if (
						field.parentNode.parentNode.parentNode.classList.contains(
							'has-errors-input'
						)
					) {
						field.parentNode.parentNode.parentNode.classList.remove(
							'has-errors-input'
						);
						field.parentNode.parentNode.parentNode.removeChild(
							field.parentNode.parentNode.parentNode.lastChild
						);
					}
				}
			}
		});
	};

	return (
		<Fragment>
			<div className='container-contact100'>
				<div className='wrap-contact100'>
					<form
						className='contact100-form validate-form'
						onSubmit={e => {
							submitForm(e);
						}}
					>
						<span className='contact100-form-title'>
							<img src='images/logo.png' alt='Logo' />
							<br />
							Fuel Release
						</span>
						<div className='wrap-input100 validate-input bg1'>
							<span className='label-input100'>CUSTOMER / Cliente *</span>
							<input
								className='input100'
								type='text'
								onChange={e => onChange(e)}
								value={customer}
								name='customer'
							/>
						</div>
						<div className='wrap-input100 validate-input bg1 rs1-wrap-input100'>
							<span className='label-input100'>Email *</span>
							<input
								className='input100'
								type='text'
								onChange={e => onChange(e)}
								value={email}
								name='email'
							/>
						</div>

						<div className='wrap-input100 bg1 rs1-wrap-input100'>
							<span className='label-input100'>
								AC REGISTRATION / Matrícula *
							</span>
							<input
								className='input100'
								type='text'
								onChange={e => onChange(e)}
								value={acRegistration}
								name='acRegistration'
							/>
						</div>
						<div className='wrap-input100 validate-input bg1 rs1-wrap-input100'>
							<span className='label-input100'>HANDLER / FBO</span>
							<input
								className='input100'
								onChange={e => onChange(e)}
								value={handler}
								name='handler'
							/>
						</div>
						<div className='wrap-input100 input100-select bg1 rs1-wrap-input100'>
							<span className='label-input100'>
								AIRCRAFT TYPE / Tipo de Aeronave *
							</span>
							<div>
								<Creatable
									ref={refAircraft}
									classNamePrefix='select'
									placeholder=''
									name='aircraftType'
									styles={customStyles}
									options={aircrafts}
									onChange={e => handleChange(e, 'aircraftType')}
									isClearable
									// onInputChange={handleInputChange}
								/>
							</div>
						</div>
						<div className='wrap-input100 validate-input bg1 rs1-wrap-input100'>
							<span className='label-input100'>
								LOCATION / Localidad (IATA - ICAO)*
							</span>
							<div>
								<AsyncSelect
									ref={refDeparture}
									classNamePrefix='select'
									placeholder=''
									name='departure'
									styles={customStyles}
									// defaultOptions={airports}
									loadOptions={loadOptions}
									cacheOptions
									onChange={e => handleChange(e, 'departure')}
									isSearchable={true}
								/>
							</div>
						</div>
						<div className='wrap-input100 validate-input bg1 rs1-wrap-input100'>
							<span className='label-input100'>
								NEXT DESTINATION / Destino Siguiente (IATA - ICAO)*
							</span>
							<div>
								<AsyncSelect
									ref={refArrival}
									classNamePrefix='select'
									placeholder=''
									name='arrival'
									styles={customStyles}
									// defaultOptions={airports}
									loadOptions={loadOptions}
									cacheOptions
									onChange={e => handleChange(e, 'arrival')}
									isSearchable={true}
								/>
							</div>
						</div>

						<div className='wrap-input100 bg1 rs1-wrap-input100'>
							<span className='label-input100'>ETA / Fecha de Llegada *</span>
							<br />
							<input
								type='date'
								name='arrivalDate'
								onChange={e => onChange(e)}
								value={arrivalDate}
								id='arrivalDate'
							/>
							<br />
							<br />
						</div>

						<div className='wrap-input100 validate-input bg1 rs1-wrap-input100'>
							<span className='label-input100'>TIME / Hora *</span>
							<div>
								<Select
									ref={refTimeA}
									classNamePrefix='select'
									placeholder=''
									name='arrivalTime'
									styles={customStyles}
									options={times}
									onChange={e => handleChange(e, 'arrivalTime')}
									isSearchable={false}
								/>
							</div>
						</div>

						<div className='wrap-input100 bg1 rs1-wrap-input100'>
							<span className='label-input100'>ETD / Fecha de Salida *</span>
							<br />
							<input
								type='date'
								name='departureDate'
								onChange={e => onChange(e)}
								value={departureDate}
								id='departureDate'
							/>
							<br />
							<br />
						</div>

						<div className='wrap-input100 validate-input bg1 rs1-wrap-input100'>
							<span className='label-input100'>TIME / Hora *</span>
							<div>
								<Select
									ref={refTimeD}
									classNamePrefix='select'
									placeholder=''
									name='departureTime'
									styles={customStyles}
									options={times}
									onChange={e => handleChange(e, 'departureTime')}
									isSearchable={false}
								/>
							</div>
						</div>

						<div className='wrap-input100 validate-input bg1 rs1-wrap-input100'>
							<span className='label-input100'>
								ESTIMATED UPLIFT / Carga estimada *
							</span>
							<input
								className='input100'
								type='text'
								onChange={e => onChange(e)}
								value={captainRequest.length === 0 ? gallons : 'NOT AVAILABLE'}
								// onKeyDown={formatInput}
								name='gallons'
								disabled={captainRequest.length !== 0}
							/>
						</div>

						<div className='wrap-input100 validate-input no-border rs1-wrap-input100'>
							<span className='label-input100'>{`At Captain's Request`}</span>

							<div className='contact100-form-radio m-t-15'>
								<input
									type='checkbox'
									id='cbx'
									style={{ display: 'none' }}
									name='captainRequest'
									value={captainRequest}
									onChange={e => radioChange(e, 'captainRequest')}
									checked={captainRequest === 'captainRequest'}
								/>
								<label htmlFor='cbx' className='toggle'>
									<span />
								</label>
							</div>
						</div>

						<div className='wrap-input100-form-radio rs1-wrap-input100'>
							<span className='label-input100'>PRODUCT / Producto *</span>

							<div className='contact100-form-radio m-t-15'>
								<input
									className='input-radio100'
									id='radio1'
									type='radio'
									onChange={e => radioChange(e, 'typeProduct')}
									name='JetA1'
									value='Jet A1'
									checked={typeProduct === 'Jet A1'}
								/>
								<label className='label-radio100' htmlFor='radio1'>
									Jet A1
								</label>
							</div>

							<div className='contact100-form-radio'>
								<input
									className='input-radio100'
									id='radio2'
									type='radio'
									onChange={e => radioChange(e, 'typeProduct')}
									name='AVGas'
									value='AV Gas'
									checked={typeProduct === 'AV Gas'}
								/>
								<label className='label-radio100' htmlFor='radio2'>
									AV Gas
								</label>
							</div>
						</div>
						<div className='wrap-input100-form-radio rs1-wrap-input100'></div>
						<div className='wrap-input100-form-radio rs1-wrap-input100'>
							<span className='label-input100'>
								FLIGHT TYPE / Tipo de Vuelo *
							</span>

              <div className='contact100-form-radio m-t-10'>
								<input
									className='input-radio100'
									id='radio3'
									type='radio'
									name='international'
									value='international'
									onChange={e => radioChange(e, 'flightType')}
									checked={flightType === 'international'}
								/>
								<label className='label-radio100' htmlFor='radio3'>
									International
								</label>
							</div>

							<div className='contact100-form-radio'>
								<input
									className='input-radio100'
									id='radio4'
									type='radio'
									name='domestic'
									value='domestic'
									onChange={e => radioChange(e, 'flightType')}
									checked={flightType === 'domestic'}
								/>
								<label className='label-radio100' htmlFor='radio4'>
									Domestic
								</label>
							</div>							
						</div>

						<div className='wrap-input100-form-radio rs1-wrap-input100'>
							<span className='label-input100'>
								OPERATION TYPE / TIPO DE OPERACION *
							</span>

							<div className='contact100-form-radio m-t-10'>
								<input
									className='input-radio100'
									id='radio5'
									type='radio'
									name='corporative'
									value='corporative'
									onChange={e => radioChange(e, 'flightOperation')}
									checked={flightOperation === 'corporative'}
								/>
								<label className='label-radio100' htmlFor='radio5'>
									Corporative/Private
								</label>
							</div>
							<div className='contact100-form-radio'>
								<input
									className='input-radio100'
									id='radio6'
									type='radio'
									name='charter'
									value='charter'
									onChange={e => radioChange(e, 'flightOperation')}
									checked={flightOperation === 'charter'}
								/>
								<label className='label-radio100' htmlFor='radio6'>
									AOC/Charter/Corp for Hire
								</label>
							</div>
						</div>

						<div className='wrap-input100 validate-input bg0'>
							<span className='label-input100'>REMARKS / Observaciones</span>
							<textarea
								className='input100'
								name='remarks'
								onChange={e => onChange(e)}
								value={remarks}
							/>
						</div>
						<div className='container-contact100-form-btn'>
							<button className='contact100-form-btn' type='submit'>
								<span>
									SEND / ENVIAR
									<i
										className='fa fa-long-arrow-right m-l-7'
										aria-hidden='true'
									/>
								</span>
							</button>
						</div>
					</form>
				</div>
			</div>
			<Modal
				title='Are you sure?'
				visible={modalData.visible}
				onOk={handleOk}
				confirmLoading={modalData.confirmLoading}
				onCancel={handleCancel}
				centered
				maskClosable={false}
			>
				<p>{modalData.text}</p>
			</Modal>
		</Fragment>
	);
};

IndexPage.propTypes = {
	dispatch: PropTypes.func.isRequired
};

export default IndexPage;
