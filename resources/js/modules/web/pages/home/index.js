// import libs
import { connect } from 'react-redux';

// import components
import IndexPage from './IndexPage';

export default connect()(IndexPage);
