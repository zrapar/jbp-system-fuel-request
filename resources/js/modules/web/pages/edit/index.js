// import libs
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

// import components
import EditPage from './EditPage';

export default withRouter(connect()(EditPage));
