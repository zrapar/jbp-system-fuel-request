/* eslint-disable no-useless-escape */
/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable no-prototype-builtins */
import React, {
	useState,
	useEffect,
	Fragment,
	useRef
	// useCallback
} from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import Creatable from 'react-select/creatable';
import AsyncSelect from 'react-select/async';
import { Redirect } from 'react-router-dom';
import {
	updateForm,
	getDataResponse,
	searchAirport,
	getAircraft,
	closeMessage
} from '../../service';
import { useSelector } from 'react-redux';
import { Modal, notification } from 'antd';
import moment from 'moment';

const key = 'EditPage';

const EditPage = ({ dispatch, match, location, history }) => {
	if (!match.params.uuid) {
		return <Redirect to='/' />;
	}
	const uuid = match.params.uuid;

	const dataEdit = useSelector(({ fuel_request }) => fuel_request.response);
	const aircrafts = useSelector(({ fuel_request }) => fuel_request.aircrafts);
	const message = useSelector(({ fuel_request }) => fuel_request.message);

	const [modalData, setModalData] = useState({
		visible: false,
		confirmLoading: false,
		text: 'Comfirm data before send it'
	});

	const [form, setForm] = useState({
		...location.state,
		captainRequest:
			location.state.gallons === `At Captain's request` ? 'captainRequest' : ''
	});

	useEffect(() => {
		dispatch(getAircraft());
		if (
			dataEdit &&
			dataEdit.hasOwnProperty('emailCostumer') &&
			!location.state
		) {
			dispatch(getDataResponse(uuid));
			setForm(dataEdit);
		}
	}, []);

	const refAircraft = useRef(null);
	const refDeparture = useRef(null);
	const refArrival = useRef(null);
	const refTimeD = useRef(null);
	const refTimeA = useRef(null);
	// refAircraft.current.state.value = null;
	// refDeparture.current.select.state.value = null;
	// refArrival.current.select.state.value = null;
	// refTimeD.current.state.value = null;
	// refTimeA.current.state.value = null;
	if (message && message.success) {
		notification['success']({
			key,
			message: 'Success',
			description: message.message,
			onClick: () => {
				notification.close(key);
			}
		});

		dispatch(closeMessage());
	}
	const loadOptions = inputValue =>
		new Promise(resolve => {
			setTimeout(() => {
				resolve(searchAirport(inputValue));
			}, 1000);
		});

	const times = [
		{ label: '00:00', value: '00:00' },
		{ label: '00:30', value: '00:30' },
		{ label: '01:00', value: '01:00' },
		{ label: '01:30', value: '01:30' },
		{ label: '02:00', value: '02:00' },
		{ label: '02:30', value: '02:30' },
		{ label: '03:00', value: '03:00' },
		{ label: '03:30', value: '03:30' },
		{ label: '04:00', value: '04:00' },
		{ label: '04:30', value: '04:30' },
		{ label: '05:00', value: '05:00' },
		{ label: '05:30', value: '05:30' },
		{ label: '06:00', value: '06:00' },
		{ label: '06:30', value: '06:30' },
		{ label: '07:00', value: '07:00' },
		{ label: '07:30', value: '07:30' },
		{ label: '08:00', value: '08:00' },
		{ label: '08:30', value: '08:30' },
		{ label: '09:00', value: '09:00' },
		{ label: '09:30', value: '09:30' },
		{ label: '10:00', value: '10:00' },
		{ label: '10:30', value: '10:30' },
		{ label: '11:00', value: '11:00' },
		{ label: '11:30', value: '11:30' },
		{ label: '12:00', value: '12:00' },
		{ label: '12:30', value: '12:30' },
		{ label: '13:00', value: '13:00' },
		{ label: '13:30', value: '13:30' },
		{ label: '14:00', value: '14:00' },
		{ label: '14:30', value: '14:30' },
		{ label: '15:00', value: '15:00' },
		{ label: '15:30', value: '15:30' },
		{ label: '16:00', value: '16:00' },
		{ label: '16:30', value: '16:30' },
		{ label: '17:00', value: '17:00' },
		{ label: '17:30', value: '17:30' },
		{ label: '18:00', value: '18:00' },
		{ label: '18:30', value: '18:30' },
		{ label: '19:00', value: '19:00' },
		{ label: '19:30', value: '19:30' },
		{ label: '20:00', value: '20:00' },
		{ label: '20:30', value: '20:30' },
		{ label: '21:00', value: '21:00' },
		{ label: '21:30', value: '21:30' },
		{ label: '22:00', value: '22:00' },
		{ label: '22:30', value: '22:30' },
		{ label: '23:00', value: '23:00' },
		{ label: '23:30', value: '23:30' }
	];

	const customStyles = {
		indicatorSeparator: () => ({ width: '0%' }),
		menu: styleBefore => ({ ...styleBefore, borderRadius: '10px' }),
		valueContainer: styleBefore => ({ ...styleBefore, padding: '0px 0px' }),
		dropdownIndicator: styleBefore => ({
			...styleBefore,
			padding: '0px',
			color: '#999'
		}),
		control: styleBefore => ({
			...styleBefore,
			backgroundColor: 'transparent !important',
			border: 'none !important'
		}),
		singleValue: styleBefore => ({
			...styleBefore,
			color: '#555555',
			fontFamily: 'Montserrat-SemiBold',
			fontSize: '18px',
			lineHeight: 1.2
		}),
		option: (styleBefore, { isFocused }) => ({
			...styleBefore,
			fontFamily: 'Montserrat-SemiBold',
			fontSize: '14px',
			lineHeight: 1.2,
			backgroundColor: isFocused ? '#212843' : 'transparent',
			color: isFocused ? '#fff' : '#555555'
		}),
		placeholder: styleBefore => ({
			...styleBefore,
			marginLeft: '0px',
			marginRight: '0px',
			color: '#999',
			fontFamily: 'Montserrat-SemiBold',
			fontSize: '18px',
			lineHeight: 1.2
		})
	};

	const submitForm = e => {
		e.preventDefault();
		const isValid = validForm();

		if (isValid) {
			setModalData({
				...modalData,
				visible: true
			});
		} else {
			notification['error']({
				key,
				message: 'Error',
				description:
					'Tienes errores en el llenado de los campos / Please check the fields for errors',
				onClick: () => {
					removeAllErrors();
					notification.close(key);
				}
			});
		}
	};
	const validateEmail = email => {
		// eslint-disable-next-line no-useless-escape
		const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email.toLowerCase());
	};

	const removeAllErrors = () => {
		Object.keys(form).map(item => {
			const field = document.getElementsByName(item)[0];
			if (
				item !== 'captainRequest' &&
				item !== 'remarks' &&
				item !== 'typeProduct' &&
				item !== 'flightOperation' &&
				item !== 'id' &&
				item !== 'uuid' &&
				item !== 'arrivalCode' &&
				item !== 'departureCode' &&
				item !== 'isCompleted' &&
				item !== 'deletedAt' &&
				item !== 'updatedAt' &&
				item !== 'handler' &&
				form[item].length === 0
			) {
				if (field.parentNode.classList.contains('wrap-input100')) {
					if (field.parentNode.classList.contains('has-errors-input')) {
						field.parentNode.classList.remove('has-errors-input');
						field.parentNode.removeChild(field.parentNode.lastChild);
					}
				} else {
					if (
						field.parentNode.parentNode.parentNode.classList.contains(
							'has-errors-input'
						)
					) {
						field.parentNode.parentNode.parentNode.classList.remove(
							'has-errors-input'
						);
						field.parentNode.parentNode.parentNode.removeChild(
							field.parentNode.parentNode.parentNode.lastChild
						);
					}
				}
			}
		});
	};

	const onChange = e => {
		let value = e.target.value;
		e.preventDefault();
		if (e.target.name === 'gallons') {
      value = value.replace(".", "");
      const re = /^([1-9][0-9]{0,4}|99999)$/;
      const pass = re.test(value);
      
      if (pass) {
        value = parseInt(value).toLocaleString().replace(",", ".").toString();
      } else if (e.target.value.length === 0) {
        value = '';
      }
		}
		setForm({ ...form, [e.target.name]: value.toUpperCase() });

		const field = document.getElementsByName(e.target.name)[0];
		if (field.parentNode.classList.contains('has-errors-input')) {
			field.parentNode.classList.remove('has-errors-input');
			field.parentNode.removeChild(field.parentNode.lastChild);
		}
	};

	const handleChange = (e, name) => {
		if (name === 'departure' || name === 'arrival') {
			setForm({
				...form,
				[name]: { uuid: e.value, label: e.label },
				[`${name}Code`]: e.value
			});
		} else {
			setForm({ ...form, [name]: e.value });
		}
		const field = document.getElementsByName(name)[0];
		if (
			field.parentNode.parentNode.parentNode.classList.contains(
				'has-errors-input'
			)
		) {
			field.parentNode.parentNode.parentNode.classList.remove(
				'has-errors-input'
			);
			field.parentNode.parentNode.parentNode.removeChild(
				field.parentNode.parentNode.parentNode.lastChild
			);
		}
	};

	const radioChange = (e, name) => {
		if (name === 'captainRequest') {
			form.captainRequest.length > 0
				? setForm({ ...form, [name]: '', gallons: '' })
				: setForm({
						...form,
						[name]: 'captainRequest',
						gallons: 'NOT AVAILABLE'
				  });
			const field = document.getElementsByName('gallons')[0];
			if (field.parentNode.classList.contains('has-errors-input')) {
				field.parentNode.classList.remove('has-errors-input');
				field.parentNode.removeChild(field.parentNode.lastChild);
			}
		} else {
			setForm({ ...form, [name]: e.target.value });
		}
	};

	const handleOk = () => {
		dispatch(updateForm(form));
		setModalData({
			...modalData,
			confirmLoading: true,
			text: 'Your request its updating...'
		});
		setTimeout(() => {
			history.push({ pathname: `/response/${form.uuid}` });
		}, 1000);
	};

	const handleCancel = () => {
		setModalData({
			...modalData,
			confirmLoading: false,
			visible: false,
			text: 'You wanna update this request? Comfirm data before send it'
		});
	};

	const validForm = () => {
		let isValid = true;
		Object.keys(form).map(item => {
			const field = document.getElementsByName(item)[0];
			if (
				item !== 'emailCostumer' &&
				item !== 'gallons' &&
				item !== 'captainRequest' &&
				item !== 'remarks' &&
				item !== 'typeProduct' &&
				item !== 'flightOperation' &&
				item !== 'id' &&
				item !== 'uuid' &&
				item !== 'arrivalCode' &&
				item !== 'departureCode' &&
				item !== 'isCompleted' &&
				item !== 'deletedAt' &&
				item !== 'updatedAt' &&
				item !== 'handler' &&
				form[item].length === 0
			) {
				if (field.parentNode.classList.contains('wrap-input100')) {
					if (!field.parentNode.classList.contains('has-errors-input')) {
						field.parentNode.classList.add('has-errors-input');
						const newSpan = document.createElement('span');
						const textError = document.createTextNode(
							`Este campo no puede estar vacio / this field cant be empty`
						);
						newSpan.appendChild(textError);
						field.parentNode.appendChild(newSpan);
					}
				} else {
					if (
						!field.parentNode.parentNode.parentNode.classList.contains(
							'has-errors-input'
						)
					) {
						field.parentNode.parentNode.parentNode.classList.add(
							'has-errors-input'
						);
						const newSpan = document.createElement('span');
						const textError = document.createTextNode(
							`Este campo no puede estar vacio / this field cant be empty`
						);
						newSpan.appendChild(textError);
						field.parentNode.parentNode.parentNode.appendChild(newSpan);
					}
				}

				isValid = false;
			}

			if (item === 'emailCostumer' && !validateEmail(form[item])) {
				if (!field.parentNode.classList.contains('has-errors-input')) {
					field.parentNode.classList.add('has-errors-input');
					const newSpan = document.createElement('span');
					const textError = document.createTextNode(
						`Este no es un email valid / this it's not a valid email`
					);
					newSpan.appendChild(textError);
					field.parentNode.appendChild(newSpan);
				}
			}

			if (
				item === 'gallons' &&
				form['captainRequest'].length === 0 &&
				form[item].length === 0
			) {
				if (!field.parentNode.classList.contains('has-errors-input')) {
					field.parentNode.classList.add('has-errors-input');
					const newSpan = document.createElement('span');
					const textError = document.createTextNode(
						`Este campo no puede estar vacio / this field cant be empty`
					);
					newSpan.appendChild(textError);
					field.parentNode.appendChild(newSpan);
				}
				isValid = false;
			}

			if (
				item === 'arrivalDate' &&
				form['departureDate'] &&
				form['arrivalDate'] &&
				form['arrivalDate'] > form['departureDate']
			) {
				if (!field.parentNode.classList.contains('has-errors-input')) {
					field.parentNode.classList.add('has-errors-input');
					const newSpan = document.createElement('span');
					const textError = document.createTextNode(
						`La fecha de llegada no puede ser mayor a la fecha de salida`
					);
					newSpan.appendChild(textError);
					field.parentNode.appendChild(newSpan);
				}
				isValid = false;
      }
      
			if (
        item === 'arrivalDate' &&
        form['arrivalDate'] &&
				moment(form['arrivalDate']).format('YYYY-MM-DD') < moment(new Date()).format('YYYY-MM-DD')
			) {
				if (!field.parentNode.classList.contains('has-errors-input')) {
					field.parentNode.classList.add('has-errors-input');
					const newSpan = document.createElement('span');
					const textError = document.createTextNode(
						`La fecha de llegada no puede ser menor a la fecha actual ${moment(new Date()).format('YYYY-MMM-DD')}`
					);
					newSpan.appendChild(textError);
					field.parentNode.appendChild(newSpan);
				}
				isValid = false;
      }
      
			if (
        item === 'departureDate' &&
        form['departureDate'] &&
				moment(form['departureDate']).format('YYYY-MM-DD') < moment(new Date()).format('YYYY-MM-DD')
			) {
				if (!field.parentNode.classList.contains('has-errors-input')) {
					field.parentNode.classList.add('has-errors-input');
					const newSpan = document.createElement('span');
					const textError = document.createTextNode(
						`La fecha de salida no puede ser menor a la fecha actual ${moment(new Date()).format('YYYY-MMM-DD')}`
					);
					newSpan.appendChild(textError);
					field.parentNode.appendChild(newSpan);
				}
				isValid = false;
			}

			if (
				item === 'departureDate' &&
				form['departureDate'] &&
				form['arrivalDate'] &&
				form['arrivalDate'] > form['departureDate']
			) {
				if (!field.parentNode.classList.contains('has-errors-input')) {
					field.parentNode.classList.add('has-errors-input');
					const newSpan = document.createElement('span');
					const textError = document.createTextNode(
						`La fecha de salida no puede ser menor a la fecha de llegada`
					);
					newSpan.appendChild(textError);
					field.parentNode.appendChild(newSpan);
				}
				isValid = false;
			}
		});

		return isValid;
	};

	return (
		<Fragment>
			<div className='container-contact100'>
				<div className='wrap-contact100'>
					<form
						className='contact100-form validate-form'
						onSubmit={e => {
							submitForm(e);
						}}
					>
						<span className='contact100-form-title'>
							<img src='images/logo.png' alt='Logo' />
							<br />
							Fuel Release
						</span>
						<div className='wrap-input100 validate-input bg1'>
							<span className='label-input100'>CUSTOMER / Cliente *</span>
							<input
								className='input100'
								type='text'
								onChange={e => onChange(e)}
								value={form.customer}
								name='customer'
							/>
						</div>
						<div className='wrap-input100 validate-input bg1 rs1-wrap-input100'>
							<span className='label-input100'>Email *</span>
							<input
								className='input100'
								type='text'
								onChange={e => onChange(e)}
								value={form.emailCostumer}
								name='emailCostumer'
							/>
						</div>

						<div className='wrap-input100 bg1 rs1-wrap-input100'>
							<span className='label-input100'>
								AC REGISTRATION / Matrícula *
							</span>
							<input
								className='input100'
								type='text'
								onChange={e => onChange(e)}
								value={form.acRegistration}
								name='acRegistration'
							/>
						</div>
						<div className='wrap-input100 validate-input bg1 rs1-wrap-input100'>
							<span className='label-input100'>Handler / FBO</span>
							<input
								className='input100'
								onChange={e => onChange(e)}
								value={form.handler}
								name='handler'
							/>
						</div>
						<div className='wrap-input100 input100-select bg1 rs1-wrap-input100'>
							<span className='label-input100'>
								AIRCRAFT TYPE / Tipo de Aeronave *
							</span>
							<div>
								<Creatable
									ref={refAircraft}
									classNamePrefix='select'
									placeholder=''
									name='aircraftType'
									styles={customStyles}
									options={aircrafts}
									isClearable
									value={
										aircrafts.filter(it => it.value === form.aircraftType)[0]
									}
									onChange={e => handleChange(e, 'aircraftType')}
								/>
							</div>
						</div>
						<div className='wrap-input100 validate-input bg1 rs1-wrap-input100'>
							<span className='label-input100'>
								LOCATION / Localidad (IATA - ICAO)*
							</span>
							<div>
								<AsyncSelect
									ref={refDeparture}
									classNamePrefix='select'
									placeholder=''
									name='departure'
									styles={customStyles}
									value={{
										value: form.departure.uuid,
										label: form.departure.label
									}}
									loadOptions={loadOptions}
									cacheOptions
									onChange={e => handleChange(e, 'departure')}
									isSearchable={true}
								/>
							</div>
						</div>
						<div className='wrap-input100 validate-input bg1 rs1-wrap-input100'>
							<span className='label-input100'>
								NEXT DESTINATION / Destino Siguiente (IATA - ICAO)*
							</span>
							<div>
								<AsyncSelect
									ref={refArrival}
									classNamePrefix='select'
									placeholder=''
									name='arrival'
									styles={customStyles}
									value={{
										value: form.arrival.uuid,
										label: form.arrival.label
									}}
									loadOptions={loadOptions}
									cacheOptions
									onChange={e => handleChange(e, 'arrival')}
									isSearchable={true}
								/>
							</div>
						</div>

						<div className='wrap-input100 bg1 rs1-wrap-input100'>
							<span className='label-input100'>ETA / Fecha de Llegada *</span>
							<br />
							<input
								type='date'
								name='arrivalDate'
								onChange={e => onChange(e)}
								value={form.arrivalDate.split(' ')[0]}
								id='arrivalDate'
							/>
						</div>

						<div className='wrap-input100 validate-input bg1 rs1-wrap-input100'>
							<span className='label-input100'>TIME / Hora *</span>
							<div>
								<Select
									ref={refTimeA}
									classNamePrefix='select'
									placeholder=''
									name='arrivalTime'
									styles={customStyles}
									value={{
										value: form.arrivalTime,
										label: form.arrivalTime
									}}
									options={times}
									onChange={e => handleChange(e, 'arrivalTime')}
									isSearchable={false}
								/>
							</div>
						</div>
						<div className='wrap-input100 bg1 rs1-wrap-input100'>
							<span className='label-input100'>ETD / Fecha de Salida *</span>
							<br />
							<input
								type='date'
								name='departureDate'
								onChange={e => onChange(e)}
								value={form.departureDate.split(' ')[0]}
								id='departureDate'
							/>
						</div>

						<div className='wrap-input100 validate-input bg1 rs1-wrap-input100'>
							<span className='label-input100'>TIME / Hora *</span>
							<div>
								<Select
									ref={refTimeD}
									classNamePrefix='select'
									placeholder=''
									name='departureTime'
									value={{
										value: form.departureTime,
										label: form.departureTime
									}}
									styles={customStyles}
									options={times}
									onChange={e => handleChange(e, 'departureTime')}
									isSearchable={false}
								/>
							</div>
						</div>

						<div className='wrap-input100 validate-input bg1 rs1-wrap-input100'>
							<span className='label-input100'>
								ESTIMATED UPLIFT / Carga estimada *
							</span>
							<input
								className='input100'
								type={form.captainRequest.length === 0 ? 'number' : 'text'}
								onChange={e => onChange(e)}
								value={
									form.captainRequest.length === 0
										? form.gallons
										: 'NOT AVAILABLE'
								}
								name='gallons'
								disabled={form.captainRequest.length !== 0}
							/>
						</div>
						<div className='wrap-input100 validate-input no-border rs1-wrap-input100'>
							<span className='label-input100'>{`At Captain's Request`}</span>

							<div className='contact100-form-radio m-t-15'>
								<input
									type='checkbox'
									id='cbx'
									style={{ display: 'none' }}
									name='captainRequest'
									value={form.captainRequest}
									onChange={e => radioChange(e, 'captainRequest')}
									checked={form.captainRequest === 'captainRequest'}
								/>
								<label htmlFor='cbx' className='toggle'>
									<span />
								</label>
							</div>
						</div>

						<div className='wrap-input100-form-radio rs1-wrap-input100 bg0'>
							<span className='label-input100'>PRODUCT / Producto *</span>

							<div className='contact100-form-radio m-t-15'>
								<input
									className='input-radio100'
									id='radio1'
									type='radio'
									onChange={e => radioChange(e, 'typeProduct')}
									name='JetA1'
									value='Jet A1'
									checked={form.typeProduct === 'Jet A1'}
								/>
								<label className='label-radio100' htmlFor='radio1'>
									Jet A1
								</label>
							</div>

							<div className='contact100-form-radio'>
								<input
									className='input-radio100'
									id='radio2'
									type='radio'
									onChange={e => radioChange(e, 'typeProduct')}
									name='AVGas'
									value='AV Gas'
									checked={form.typeProduct === 'AV Gas'}
								/>
								<label className='label-radio100' htmlFor='radio2'>
									AV Gas
								</label>
							</div>
						</div>
						<div className='wrap-input100-form-radio rs1-wrap-input100'></div>

						<div className='wrap-input100-form-radio rs1-wrap-input100'>
							<span className='label-input100'>
								FLIGHT TYPE / Tipo de Vuelo *
							</span>

              <div className='contact100-form-radio m-t-10'>
								<input
									className='input-radio100'
									id='radio3'
									type='radio'
									name='international'
									value='international'
									onChange={e => radioChange(e, 'flightType')}
									checked={form.flightType === 'international'}
								/>
								<label className='label-radio100' htmlFor='radio3'>
									International
								</label>
							</div>

							<div className='contact100-form-radio'>
								<input
									className='input-radio100'
									id='radio4'
									type='radio'
									name='domestic'
									value='domestic'
									onChange={e => radioChange(e, 'flightType')}
									checked={form.flightType === 'domestic'}
								/>
								<label className='label-radio100' htmlFor='radio4'>
									Domestic
								</label>
							</div>
						</div>
						<div className='wrap-input100-form-radio rs1-wrap-input100'>
							<span className='label-input100'>
								OPERATION TYPE / Tipo de Operacion *
							</span>

							<div className='contact100-form-radio m-t-15'>
								<input
									className='input-radio100'
									id='radio5'
									type='radio'
									name='corporative'
									value='corporative'
									onChange={e => radioChange(e, 'flightOperation')}
									checked={form.flightOperation === 'corporative'}
								/>
								<label className='label-radio100' htmlFor='radio5'>
									Corporative/Private
								</label>
							</div>
							<div className='contact100-form-radio'>
								<input
									className='input-radio100'
									id='radio6'
									type='radio'
									name='charter'
									value='charter'
									onChange={e => radioChange(e, 'flightOperation')}
									checked={form.flightOperation === 'charter'}
								/>
								<label className='label-radio100' htmlFor='radio6'>
									AOC/Charter/Corp for Hire
								</label>
							</div>
						</div>

						<div className='wrap-input100 validate-input bg0'>
							<span className='label-input100'>REMARKS / Observaciones</span>
							<textarea
								className='input100'
								name='remarks'
								onChange={e => onChange(e)}
								value={form.remarks ? form.remarks : ''}
							/>
						</div>
						<div className='container-contact100-form-btn'>
							<button className='contact100-form-btn' type='submit'>
								<span>
									GUARDAR CAMBIOS / SAVE CHANGES
									<i
										className='fa fa-long-arrow-right m-l-7'
										aria-hidden='true'
									/>
								</span>
							</button>
						</div>
					</form>
				</div>
			</div>
			<Modal
				title='Are you sure?'
				visible={modalData.visible}
				onOk={handleOk}
				confirmLoading={modalData.confirmLoading}
				onCancel={handleCancel}
				centered
				maskClosable={false}
			>
				<p>{modalData.text}</p>
			</Modal>
		</Fragment>
	);
};

EditPage.propTypes = {
	dispatch: PropTypes.func.isRequired,
	match: PropTypes.object.isRequired,
	location: PropTypes.object.isRequired,
	history: PropTypes.object.isRequired
};

export default EditPage;
