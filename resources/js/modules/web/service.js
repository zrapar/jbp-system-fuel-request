import Http from '../../utils/Http';
import Transformer from '../../utils/Transformer';
import * as formActions from './store/actions';
import _ from 'lodash';

export const sendForm = formData => async dispatch => {
	const { data } = await Http.post('fuel/send-form', formData);
	const response = Transformer.fetch(data);
	dispatch(formActions.showMessage(response));
};
export const updateForm = formData => async dispatch => {
	const { data } = await Http.post(`fuel/update/${formData.uuid}`, formData);
	const response = Transformer.fetch(data);
	dispatch(formActions.showMessage(response));
};

export const getAirports = () => async dispatch => {
	let array = [];
	const { data } = await Http.get('airport');
	const response = Transformer.fetch(data);

	_.forEach(response.data.data, function(value) {
		array.push({
			label: value.label,
			value: value.uuid
		});
	});
	dispatch(formActions.setAirports({ data: array, success: data.success }));
};

export const getAircraft = () => async dispatch => {
	let array = [];
	const { data } = await Http.get('airport/aircraft');
	const response = Transformer.fetch(data);

	_.forEach(response.data, function(value) {
		array.push({
			label: value.type,
			value: value.uuid
		});
	});
	dispatch(formActions.setAircrafts({ data: array, success: data.success }));
};

export const searchAirport = value => {
	return Http.get(`airport/search?search_by=${value}`)
		.then(res => {
			return Transformer.fetch(res.data);
		})
		.catch(err => {
			console.log(err);
		});
};

export const getSuppliers = () => async dispatch => {
	let array = [];
	const { data } = await Http.get('suppliers/all');
	const response = Transformer.fetch(data);
	_.forEach(response.data, value => {
		array.push({
			label: value.supplier,
			value: value.uuid
		});
	});

	dispatch(formActions.setSuppliers({ data: array, success: data.success }));
};

export const getClients = () => async dispatch => {
	let array = [];
	const { data } = await Http.get('clients/all');
	const response = Transformer.fetch(data);
	_.forEach(response.data, value => {
		array.push({
			label: value.client,
			value: value.uuid
		});
	});

	dispatch(formActions.setClients({ data: array, success: data.success }));
};
export const getAgencies = () => async dispatch => {
	let array = [];
	const { data } = await Http.get('agencies/all');
	const response = Transformer.fetch(data);
	_.forEach(response.data, value => {
		array.push({
			label: value.agency,
			value: value.uuid
		});
	});

	dispatch(formActions.setAgencies({ data: array, success: data.success }));
};

export const getDataResponse = uuid => async dispatch => {
	const { data } = await Http.post('fuel/get-response', {
		uuid: uuid
	});

	const response = Transformer.fetch(data);

	dispatch(formActions.getResponseData(response));
	dispatch(formActions.lastInvoice(response.lastInvoiceNumber));
};

export const sendCompleteForm = formData => async dispatch => {
	try {
		const { data } = await Http.post('fuel/complete-form', formData);
		const response = Transformer.fetch(data);
		dispatch(formActions.showMessageComplete(response));
	} catch ({ response, request }) {
		if (response && response.data) {
			dispatch(formActions.showMessageComplete(response));
		}
	}
};

export const deleteFuelRequest = uuid => async dispatch => {
	const { data } = await Http.post('fuel/delete-request', { uuid });
	const response = Transformer.fetch(data);
	console.log(response);
	dispatch(formActions.showMessageDelete(response));
};

export const closeMessageDelete = () => async dispatch => {
	dispatch(formActions.closeMessageDeleteAction());
};
export const closeMessage = () => async dispatch => {
	dispatch(formActions.closeMessage());
};
export const showToast = response => async dispatch => {
	dispatch(formActions.showMessageComplete(response));
};

/**
 * fetch the current logged in user
 *
 * @returns {function(*)}
 */
// export function fetchUser() {
// 	return dispatch => {
// 		return Http.get('auth/user')
// 			.then(res => {
// 				const data = Transformer.fetch(res.data);
// 				dispatch(authActions.authUser(data));
// 			})
// 			.catch(err => {
// 				console.log(err);
// 			});
// 	};
// }

// /**
//  * login user
//  *
//  * @param credentials
//  * @returns {function(*)}
//  */
// export function login(credentials) {
// 	return dispatch =>
// 		new Promise((resolve, reject) => {
// 			Http.post('auth/login', credentials)
// 				.then(res => {
// 					const data = Transformer.fetch(res.data);
// 					dispatch(authActions.authLogin(data.accessToken));
// 					return resolve();
// 				})
// 				.catch(err => {
// 					const statusCode = err.response.status;
// 					const data = {
// 						error: null,
// 						statusCode
// 					};

// 					if (statusCode === 422) {
// 						const resetErrors = {
// 							errors: err.response.data.errors,
// 							replace: false,
// 							searchStr: '',
// 							replaceStr: ''
// 						};
// 						data.error = Transformer.resetValidationFields(
// 							resetErrors
// 						);
// 					} else if (statusCode === 401) {
// 						data.error = err.response.data.message;
// 					}
// 					return reject(data);
// 				});
// 		});
// }

// export function register(credentials) {
// 	return dispatch =>
// 		new Promise((resolve, reject) => {
// 			Http.post('auth/register', Transformer.send(credentials))
// 				.then(res => {
// 					const data = Transformer.fetch(res.data);
// 					dispatch(authActions.authLogin(data.accessToken));
// 					return resolve();
// 				})
// 				.catch(err => {
// 					const statusCode = err.response.status;
// 					const data = {
// 						error: null,
// 						statusCode
// 					};

// 					if (statusCode === 422) {
// 						const resetErrors = {
// 							errors: err.response.data.errors,
// 							replace: false,
// 							searchStr: '',
// 							replaceStr: ''
// 						};
// 						data.error = Transformer.resetValidationFields(
// 							resetErrors
// 						);
// 					} else if (statusCode === 401) {
// 						data.error = err.response.data.message;
// 					}
// 					console.log(data);
// 					return reject(data);
// 				});
// 		});
// }

// /**
//  * logout user
//  *
//  * @returns {function(*)}
//  */
// export function logout() {
// 	return dispatch => {
// 		return Http.delete('auth/logout')
// 			.then(() => {
// 				dispatch(authActions.authLogout());
// 			})
// 			.catch(err => {
// 				console.log(err);
// 			});
// 	};
// }
