// import lib
import Loadable from 'react-loadable';

// import components
import LoadingComponent from '../../common/loader/index';

const routes = [
	{
		path: '/',
		exact: true,
		component: Loadable({
			loader: () => import('./pages/home/index'),
			loading: LoadingComponent
		})
	},
	{
		path: '/edit/:uuid',
		exact: true,
		auth: true,
		component: Loadable({
			loader: () => import('./pages/edit/index'),
			loading: LoadingComponent
		})
	},
	{
		path: '/response/:uuid',
		exact: true,
		auth: true,
		component: Loadable({
			loader: () => import('./pages/response/index'),
			loading: LoadingComponent
		})
	}
];

export default routes;
