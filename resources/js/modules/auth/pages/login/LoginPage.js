import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { login } from '../../service';
import { Spin, notification } from 'antd';



const openNotification = (title, message, type) => {
	notification[type]({
		message: title,
		description: message
	});
};

const LoginPage = ({ dispatch, isAuthenticated, failed, messageFailed }) => {
	if (isAuthenticated) {
		return <Redirect to='/dashboard' />;
	}

	const [formData, setFormData] = useState({ email: '', password: '' });
	
  useEffect( () => {
    if (messageFailed) {
      if(messageFailed.error.email) openNotification('Error', messageFailed.error.email, 'error');
      if(messageFailed.error.password) openNotification('Error', messageFailed.error.password, 'error');
    }
  },[messageFailed])

	const { email, password } = formData;

	const submitForm = e => {
		e.preventDefault();
		dispatch(login({ email, password }));
	};

	const onChange = e => {
		e.preventDefault();
		setFormData({ ...formData, [e.target.name]: e.target.value });
	};

	const formValid = () => {
		return email.length > 0 && password.length > 0;
	};
  
  return (
		<Spin spinning={failed} tip='Loading...'>
			<div className='container-contact100'>
				<div className='wrap-contact100'>
					<form
						className='contact100-form validate-form'
						onSubmit={e => {
							submitForm(e);
						}}
					>
						<span className='contact100-form-title'>
							<img src='images/logo.png' alt='Logo' />
							<br />
							Fuel Release
						</span>

						<div className='wrap-input100 validate-input bg1 rs1-wrap-input100'>
							<span className='label-input100'>Email *</span>
							<input
								className='input100'
								type='text'
								onChange={e => onChange(e)}
								value={email}
								name='email'
								placeholder='user@testmail.com'
							/>
						</div>

						<div className='wrap-input100 validate-input bg1 rs1-wrap-input100'>
							<span className='label-input100'>Contraseña</span>
							<input
								className='input100'
								type='password'
								onChange={e => onChange(e)}
								value={password}
								name='password'
								placeholder='***********'
							/>
						</div>

						<div className='container-contact100-form-btn'>
							<button
								className='contact100-form-btn'
								type='submit'
								disabled={!formValid()}
							>
								<span>
									LOGIN IN
									<i
										className='fa fa-long-arrow-right m-l-7'
										aria-hidden='true'
									/>
								</span>
							</button>
						</div>
					</form>
				</div>
			</div>
		</Spin>
	);
};
LoginPage.propTypes = {
	isAuthenticated: PropTypes.bool.isRequired,
	failed: PropTypes.bool.isRequired,
	dispatch: PropTypes.func.isRequired
};

export default LoginPage;
