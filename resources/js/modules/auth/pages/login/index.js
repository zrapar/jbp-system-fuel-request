// import libs
import { connect } from 'react-redux';

// import components
import LoginPage from './LoginPage';

const mapStateToProps = state => {
	return {
		isAuthenticated: state.auth.isAuthenticated,
		failed: state.auth.failed,
		messageFailed: state.auth.messageFailed
	};
};

export default connect(mapStateToProps)(LoginPage);
