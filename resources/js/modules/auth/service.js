import Http from '../../utils/Http';
import * as authActions from './store/actions';
import Transformer from '../../utils/Transformer';

/**
 * fetch the current logged in user
 *
 * @returns {function(*)}
 */
export const fetchUser = () => async dispatch => {
	const { data } = await Http.get('auth/user');
	const response = Transformer.fetch(data);
	dispatch(authActions.authUser(response));
};

const loginFetch = () => async dispatch => {
	const { data } = await Http.get('auth/user');
	const response = Transformer.fetch(data);
	dispatch(authActions.authUser(response));
};

/**
 * login user
 *
 * @param credentials
 * @returns {function(*)}
 */
export const login = credentials => async dispatch => {
	dispatch(authActions.startAuth());
	try {
		const { data } = await Http.post('auth/login', credentials);
		const response = Transformer.fetch(data);
		dispatch(authActions.authLogin(response.accessToken));
		dispatch(loginFetch());
	} catch (err) {
    const statusCode = err.response.status;
    const data = {
      error: null,
      statusCode
    };
    if (statusCode === 422) {
      const resetErrors = {
        errors: err.response.data.errors,
        replace: false,
        searchStr: '',
        replaceStr: ''
      };
      data.error = Transformer.resetValidationFields(resetErrors);
    } else if (statusCode === 401) {
      data.error = err.response.data.message;
    }
    dispatch(authActions.authFail(data));
	}
};

export function register(credentials) {
	return dispatch =>
		new Promise((resolve, reject) => {
			Http.post('auth/register', Transformer.send(credentials))
				.then(res => {
					const data = Transformer.fetch(res.data);
					dispatch(authActions.authLogin(data.accessToken));
					return resolve();
				})
				.catch(err => {
					const statusCode = err.response.status;
					const data = {
						error: null,
						statusCode
					};

					if (statusCode === 422) {
						const resetErrors = {
							errors: err.response.data.errors,
							replace: false,
							searchStr: '',
							replaceStr: ''
						};
						data.error = Transformer.resetValidationFields(resetErrors);
					} else if (statusCode === 401) {
						data.error = err.response.data.message;
					}
					console.log(data);
					return reject(data);
				});
		});
}

/**
 * logout user
 *
 * @returns {function(*)}
 */
export function logout() {
	return dispatch => {
		return Http.delete('auth/logout')
			.then(() => {
				dispatch(authActions.authLogout());
			})
			.catch(err => {
				console.log(err);
			});
	};
}
