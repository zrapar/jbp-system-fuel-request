// import libs
import React from 'react';
import { Router, Switch, Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

// import components
import routes from './routes';
import PrivateRoute from './Private';
import PublicRoute from './Public';

// import Layout from '../layout';

const Routes = ({ history }) => (
	<Router history={history}>
		<Switch>
			{routes.map((route, i) => {
				if (route.auth) {
					return <PrivateRoute key={i} {...route} />;
				}
				return <PublicRoute key={i} {...route} />;
			})}
			<Route render={() => <Redirect to='/' />} />
		</Switch>
	</Router>
);

Routes.propTypes = {
	history: PropTypes.object.isRequired
};

export default Routes;
