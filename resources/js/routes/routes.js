// import modular routes
import webRoutes from '../modules/web/routes';
import authRoutes from '../modules/auth/routes';
import userRoutes from '../modules/user/routes';
import dashboardRoutes from '../modules/dashboard/routes';

export default [...webRoutes, ...authRoutes, ...userRoutes, ...dashboardRoutes];
