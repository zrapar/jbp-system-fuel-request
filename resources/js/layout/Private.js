//import libs
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

// import components
// import Navigation from '../common/navigation/index';
// import ScrollTop from '../common/scroll-top/index';
// import Footer from '../common/footer/index';

// const containerStyle = {
// 	paddingTop: '3.5rem'
// };

const displayName = 'Private Layout';
const propTypes = {
	children: PropTypes.node.isRequired
};

function PrivateLayout({ children }) {
	return <Fragment>{children}</Fragment>;
}

PrivateLayout.dispatch = displayName;
PrivateLayout.propTypes = propTypes;

export default PrivateLayout;
