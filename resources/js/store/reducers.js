import { combineReducers } from 'redux';

import auth from '../modules/auth/store/reduer';
import user from '../modules/user/store/reducer';
import fuel_request from '../modules/web/store/reducer';
import dashboard from '../modules/dashboard/store/reducer';

export default combineReducers({ auth, user, fuel_request, dashboard });
