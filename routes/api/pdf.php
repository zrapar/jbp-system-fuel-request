<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'PDFController@getRequests')->name('pdf.get-requests')->middleware('auth:api');
Route::post('send-form', 'PDFController@getDataForm')->name('pdf.post-data');
Route::post('/update/{uuid}', 'PDFController@updateRequest')->name('pdf.post-data');
Route::post('get-response', 'PDFController@getDataToResponse')->name('pdf.get-response');
Route::post('complete-form', 'PDFController@postConfirmedData')->name('pdf.complete-form');
Route::post('delete-request', 'PDFController@deleteRequest')->name('pdf.delete-request');
Route::patch('/send-mail/{uuid}', 'PDFController@sendMailFromDashboard')->name('pdf.send-mail');