<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'AgenciesController@index')->name('agencies.get-data')->middleware('auth:api');
Route::get('/client/{id}', 'AgenciesController@show')->name('agencies.get-single-agency')->middleware('auth:api');
Route::get('/all', 'AgenciesController@getAll')->name('agencies.get-all-agency')->middleware('auth:api');
Route::post('/create', 'AgenciesController@store')->name('agencies.create')->middleware('auth:api');
Route::put('/update/{uuid}', 'AgenciesController@update')->name('agencies.update')->middleware('auth:api');
Route::delete('/delete/{uuid}', 'AgenciesController@destroy')->name('agencies.delete')->middleware('auth:api');