<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'SuppliersController@index')->name('handlers.get-data')->middleware('auth:api');
Route::get('/handler/{id}', 'SuppliersController@show')->name('handlers.get-single-handler')->middleware('auth:api');
Route::get('/all', 'SuppliersController@getAll')->name('handlers.get-all-handler')->middleware('auth:api');
Route::post('/create', 'SuppliersController@store')->name('handlers.create')->middleware('auth:api');
Route::put('/update/{uuid}', 'SuppliersController@update')->name('handlers.update')->middleware('auth:api');
Route::delete('/delete/{uuid}', 'SuppliersController@destroy')->name('handlers.delete')->middleware('auth:api');