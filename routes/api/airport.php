<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'AirportController@getAirport')->name('airport.get');
Route::get('/aircraft', 'AirportController@getAircraft')->name('airport.get');
Route::get('/search/{search_by?}', 'AirportController@searchAirport')->name('airport.search');