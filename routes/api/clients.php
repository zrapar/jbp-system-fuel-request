<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'ClientsController@index')->name('clients.get-data')->middleware('auth:api');
Route::get('/client/{id}', 'ClientsController@show')->name('clients.get-client-handler')->middleware('auth:api');
Route::get('/all', 'ClientsController@getAll')->name('clients.get-all-client')->middleware('auth:api');
Route::post('/create', 'ClientsController@store')->name('clients.create')->middleware('auth:api');
Route::put('/update/{uuid}', 'ClientsController@update')->name('clients.update')->middleware('auth:api');
Route::delete('/delete/{uuid}', 'ClientsController@destroy')->name('clients.delete')->middleware('auth:api');