<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Airports extends Model
{
    protected $table = 'airports';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = ['uuid','label','iata','icao'];
}
