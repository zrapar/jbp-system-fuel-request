<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    // uuid
    // id_invoice
    // handler
    // owner
    // from
    // request_uuid

    // use soft delete instead of permanent delete
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'invoices';

    protected $fillable = [
        'uuid',
        'id_invoice',
        'supplier',
        'owner',
        'from',
        'request_uuid',
    ];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    public function request()
    {
        return $this->hasOne('App\Models\FuelRequest', 'uuid', 'request_uuid');
    }
    public function suppliers()
    {
        return $this->hasOne('App\Models\Suppliers', 'uuid', 'supplier');
    }
    public function clients()
    {
        return $this->hasOne('App\Models\Clients', 'uuid', 'owner');
    }
    public function agencies()
    {
        return $this->hasOne('App\Models\Agencies', 'uuid', 'from');
    }
}