<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Suppliers extends Model
{
    // uuid
    // supplier
    // supplier_email
    // attn
    // attn_email
    // cc
    // cc_email
    use SoftDeletes;

    protected $table = 'suppliers';

    protected $fillable = [
        'uuid',
        'supplier',
        'supplier_email',
        'attn',
        'attn_email',
        'cc',
        'cc_email'
    ];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at', 'created_at', 'updated_at'
    ];

    public function invoices()
    {
        return $this->hasMany('App\Models\Invoice', 'uuid', 'supplier');
    }
}