<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
// use Illuminate\Contracts\Pagination\Paginator;
// use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FuelRequest extends Model
{
  // use soft delete instead of permanent delete
  use SoftDeletes;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'fuel_request';

  protected $fillable = [
    'uuid',
    'ac_registration',
    'aircraft_type',
    'arrival_code',
    'arrival_date',
    'arrival_time',
    'customer',
    'handler',
    'departure_code',
    'departure_date',
    'departure_time',
    'flight_operation',
    'flight_type',
    'gallons',
    'remarks',
    'type_product',
    'email_costumer',
    'send_mail'
  ];


  /**
   * The attributes that should be mutated to dates.
   *
   * @var array
   */
  protected $dates = [
    'deleted_at',
    'arrival_date',
    'departure_date'
  ];

  public function arrival()
  {
    return $this->hasOne('App\Models\Airports', 'uuid', 'arrival_code');
  }

  public function departure()
  {
    return $this->hasOne('App\Models\Airports', 'uuid', 'departure_code');
  }

  public function invoice()
  {
    return $this->hasOne('App\Models\Invoice', 'request_uuid', 'uuid');
  }

  public function aircraft()
  {
    return $this->hasOne('App\Models\AircraftTypes', 'uuid', 'aircraft_type');
  }
}