<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Clients extends Model
{
    use SoftDeletes;

    protected $table = 'clients';

    protected $fillable = [
        'uuid',
        'client',
        'client_email',
        'attn_email',
        'cc_email'
    ];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at', 'created_at', 'updated_at'
    ];

    public function invoices()
    {
        return $this->hasMany('App\Models\Invoice', 'uuid', 'owner');
    }
}