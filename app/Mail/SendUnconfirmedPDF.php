<?php

namespace App\Mail;

use App\Models\FuelRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPDF extends Mailable
{
  use Queueable, SerializesModels;

  public $FuelRequest;
  public $pdf;
  public $type;
  protected $nameInvoice;
  protected $name;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct(array $data, $pdf, $name)
  {
    $this->FuelRequest = $data;
    $this->pdf = $pdf;
    $this->type = 'email';
    $this->nameInvoice = "{$name}.pdf";
    $this->name = $name;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    return $this->view('pdfview')
      ->subject($this->name)
      ->attachData($this->pdf, $this->nameInvoice, [
        'mime' => 'application/pdf',
      ]);
  }
}