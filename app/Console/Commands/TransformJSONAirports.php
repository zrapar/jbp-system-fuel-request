<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Ramsey\Uuid\Uuid;
use Storage;

class TransformJSONAirports extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'airports:transform';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Transform Aiports JSON';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $array = [];
    $data = json_decode(Storage::get('airport-codes.json'));
    $bar = $this->output->createProgressBar(count($data));
    $bar->start();
    foreach ($data as $key => $json) {
      if (strpos($json->type, 'airport') !== false && isset($json->iata_code) && isset($json->gps_code) && $json->iata_code != '0' && $json->gps_code != '-' && $json->iata_code != '0' && $json->gps_code != '-') {
        $name = utf8_decode($json->name);
        $municipality = utf8_decode($json->municipality);
        array_push($array, [
          'uuid' => Uuid::uuid4()->toString(),
          'label' => "{$json->iata_code} - {$json->gps_code} ({$name} - {$municipality}, {$json->iso_country})",
          'iata_code' => $json->iata_code,
          'icao_code' => $json->gps_code,
          'type' => $json->type
        ]);
      }
      $bar->advance();
    }
    Storage::put('filter-json.json', json_encode($array));
    $bar->finish();
  }
}