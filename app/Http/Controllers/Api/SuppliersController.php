<?php

namespace App\Http\Controllers\API;

use Validator;
use Ramsey\Uuid\Uuid;
use App\Models\Suppliers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SuppliersController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    try {
      $Suppliers = Suppliers::with(['invoices'])->paginate(15);
      if (!$Suppliers) {
        return response()->json([
          'message' => 'Error',
          'success' => false
        ], 401);
      }
      return response()->json([
        'data' => $Suppliers,
        'success' => true
      ], 200);
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      $rules = [
        'supplier' => ['required'],
        'supplierEmail' => ['required', 'regex:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'],
      ];

      $msg = [
        'supplier.required' => 'No se envio el nombre del supplier / the field "supplier" was not sent',
        'supplierEmail.required' => 'No se envio el correo del supplier / the field "supplier email" was not sent ',
        'supplierEmail.regex' => 'Ya existe este email en la base de datos / this email exists already',
      ];

      if (isset($request->attnEmail)) {
        $rules['attnEmail'] = ['regex:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'];
        $rules['attn'] = ['required'];
        $msg['attnEmail.regex'] = 'No es un email valido / this not a valid email';
        $msg['attn.required'] = "Se neceita el nombre del Attn / Attns name its needed";
      }

      if (isset($request->ccEmail)) {
        $rules['ccEmail'] = ['regex:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'];
        $rules['cc'] = ['required'];
        $msg['ccEmail.regex'] = 'No es un email valido / this not a valid email';
        $msg['cc.required'] = "Se neceita el nombre del CC / CC name its needed";
      }

      $validator = Validator::make($request->all(), $rules, $msg);

      if ($validator->fails()) {
        return response()->json([
          'errors' => $validator->errors(),
          'success' => false
        ], 404);
      }

      $Suppliers = Suppliers::create([
        'uuid' => Uuid::uuid4()->toString(),
        'supplier' => $request->supplier,
        'supplier_email' => $request->supplierEmail
      ]);


      if (isset($request->attnEmail) && isset($request->attn)) {
        $Suppliers->attn_email = $request->attnEmail;
        $Suppliers->attn = $request->attn;
      }

      if (isset($request->ccEmail) && isset($request->cc)) {
        $Suppliers->cc_email = $request->ccEmail;
        $Suppliers->cc = $request->cc;
      }

      if ($Suppliers->save()) {

        return response()->json([
          'message' => 'Se genero una nuevo handler',
          'success' => true,
          'data' => $Suppliers
        ], 201);
      }
      return response()->json([
        'message' => 'Ha ocurrido un error',
        'success' => false
      ], 500);
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\Suppliers  $Suppliers
   * @return \Illuminate\Http\Response
   */
  public function show(Suppliers $Suppliers)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Models\Suppliers  $Suppliers
   * @return \Illuminate\Http\Response
   */
  public function getAll()
  {
    try {
      $Suppliers = Suppliers::all();
      if (!$Suppliers) {
        return response()->json([
          'message' => 'Error',
          'success' => false
        ], 401);
      }
      return response()->json([
        'data' => $Suppliers,
        'success' => true
      ], 200);
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\Suppliers  $Suppliers
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Suppliers $Suppliers)
  {
    try {

      $Supplier = $Suppliers->where('uuid', $request->uuid)->first();

      if (!$Supplier) {
        return response()->json([
          'message' => 'No existe el hanlder asociado',
          'success' => false
        ], 404);
      }

      $rules = [
        'supplier' => ['required'],
        'supplierEmail' => ['required', 'regex:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'],
      ];

      $msg = [
        'supplier.required' => 'No se envio el nombre del supplier / the field "supplier" was not sent',
        'supplierEmail.required' => 'No se envio el correo del supplier / the field "supplier email" was not sent ',
        'supplierEmail.regex' => 'Ya existe este email en la base de datos / this email exists already',
      ];

      if (isset($request->attnEmail) && $Supplier->attn_email != $request->attnEmail) {
        $rules['attnEmail'] = ['regex:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'];
        $rules['attn'] = ['required'];
        $msg['attnEmail.regex'] = 'No es un email valido / this not a valid email';
        $msg['attn.required'] = "Se neceita el nombre del Attn / Attns name its needed";
      }

      if (isset($request->ccEmail) && $Supplier->cc_email != $request->ccEmail) {
        $rules['ccEmail'] = ['regex:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'];
        $rules['cc'] = ['required'];
        $msg['ccEmail.regex'] = 'No es un email valido / this not a valid email';
        $msg['cc.required'] = "Se neceita el nombre del CC / CC name its needed";
      }

      $validator = Validator::make($request->all(), $rules, $msg);

      if ($validator->fails()) {
        return response()->json([
          'errors' => $validator->errors(),
          'success' => false
        ], 404);
      }

      $Supplier->fill([
        'supplier' => $request->supplier,
        'supplier_email' => $request->supplierEmail
      ]);

      if (isset($request->attnEmail) && isset($request->attn)) {
        $Supplier->attn_email = $request->attnEmail;
        $Supplier->attn = $request->attn;
      }

      if (isset($request->ccEmail) && isset($request->cc)) {
        $Supplier->cc = $request->cc;
        $Supplier->cc_email = $request->ccEmail;
      }

      if ($Supplier->save()) {
        return response()->json([
          'message' => 'El Supplier fue actualizado correctamente',
          'success' => true,
          'data' => $Supplier
        ], 201);
      }
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\Suppliers  $Suppliers
   * @return \Illuminate\Http\Response
   */
  public function destroy($uuid, Suppliers $Suppliers)
  {
    try {
      $Handler = $Suppliers->where('uuid', $uuid)->first();
      if (!$Handler) {
        return response()->json([
          'message' => 'No existe el handler asociado',
          'success' => false
        ], 404);
      }

      $Handler->forceDelete();

      return response()->json([
        'message' => 'El Handler fue borrado correctamente',
        'success' => true,
      ], 200);
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }
}