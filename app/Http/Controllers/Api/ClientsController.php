<?php

namespace App\Http\Controllers\Api;

use Validator;
use Ramsey\Uuid\Uuid;
use App\Models\Clients;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientsController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    try {
      $Clients = Clients::with(['invoices'])->paginate(15);
      if (!$Clients) {
        return response()->json([
          'message' => 'Error',
          'success' => false
        ], 401);
      }
      return response()->json([
        'data' => $Clients,
        'success' => true
      ], 200);
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {

    try {

      $rules = [
        'client' => 'required',
        'clientEmail' => ['required', 'regex:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'],
      ];

      $msg = [
        'client.required' => 'No se envio el dueño de la areonave / the field "client" was not sent',
        'clientEmail.required' => 'No se envio el correo / the field "client email" was not sent ',
        'clientEmail.regex' => 'Email invalido / this email its invalid'
      ];

      if (isset($request->attnEmail)) {
        $rules['attnEmail'] = ['regex:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'];
        $msg['attnEmail.regex'] = 'Email invalido / this email its invalid';
      }

      if (isset($request->ccEmail)) {
        $rules['ccEmail'] = ['regex:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'];
        $msg['ccEmail.regex'] = 'Email invalido / this email its invalid';
      }

      $validator = Validator::make($request->all(), $rules, $msg);

      if ($validator->fails()) {
        return response()->json([
          'errors' => $validator->errors(),
          'success' => false
        ], 404);
      }

      $Clients = Clients::create([
        'uuid' => Uuid::uuid4()->toString(),
        'client' => $request->client,
        'client_email' => $request->clientEmail,
      ]);

      if (isset($request->attnEmail)) {
        $Clients['attn_email'] = $request->attnEmail;
      }

      if (isset($request->ccEmail)) {
        $Clients['cc_email'] = $request->ccEmail;
      }

      if ($Clients->save()) {

        return response()->json([
          'message' => 'Se guardo un nuevo cliente',
          'success' => true,
          'data' => $Clients
        ], 201);
      }

      return response()->json([
        'message' => 'Ha ocurrido un error',
        'success' => false
      ], 500);
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\Clients  $clients
   * @return \Illuminate\Http\Response
   */
  public function show(Clients $clients)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Models\Clients  $clients
   * @return \Illuminate\Http\Response
   */
  public function getAll()
  {
    try {
      $Clients = Clients::all();
      if (!$Clients) {
        return response()->json([
          'message' => 'Error',
          'success' => false
        ], 401);
      }
      return response()->json([
        'data' => $Clients,
        'success' => true
      ], 200);
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\Clients  $clients
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Clients $clients)
  {
    try {
      $Client = $clients->where('uuid', $request->uuid)->first();

      if (!$Client) {
        return response()->json([
          'message' => 'No existe el cliente asociado',
          'success' => false
        ], 404);
      }

      $rules = [
        'client' => 'required',
        'clientEmail' => ['required', 'regex:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'],
      ];

      $msg = [
        'client.required' => 'No se envio el dueño de la areonave / the field "client" was not sent',
        'clientEmail.required' => 'No se envio el correo / the field "client email" was not sent ',
        'clientEmail.regex' => 'Email invalido / this email its invalid',
      ];

      if (isset($request->attnEmail) && $Client->attn_email != $request->attnEmail) {
        $rules['attnEmail'] = ['regex:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'];
        $msg['attnEmail.regex'] = 'Email invalido / this email its invalid';
      }

      if (isset($request->ccEmail) && $Client->cc_email != $request->ccEmail) {
        $rules['ccEmail'] = ['regex:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'];
        $msg['ccEmail.regex'] = 'Email invalido / this email its invalid';
      }

      $validator = Validator::make($request->all(), $rules, $msg);

      if ($validator->fails()) {
        return response()->json([
          'errors' => $validator->errors(),
          'success' => false
        ], 404);
      }

      $Client->fill([
        'client' => $request->client,
        'client_email' => $request->clientEmail,
      ]);

      if (isset($request->attnEmail)) {
        $Client['attn_email'] = $request->attnEmail;
      }
      if (isset($request->ccEmail)) {
        $Client['cc_email'] = $request->ccEmail;
      }

      if ($Client->save()) {
        return response()->json([
          'message' => 'El Cliente fue actualizado correctamente',
          'success' => true,
          'data' => $Client
        ], 201);
      }
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\Clients  $clients
   * @return \Illuminate\Http\Response
   */
  public function destroy($uuid, Clients $clients)
  {
    try {
      $Client = $clients->where('uuid', $uuid)->first();
      if (!$Client) {
        return response()->json([
          'message' => 'No existe la cliente asociada',
          'success' => false
        ], 404);
      }

      $Client->forceDelete();

      return response()->json([
        'message' => 'El cliente fue borrado correctamente',
        'success' => true,
      ], 200);
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }
}