<?php

namespace App\Http\Controllers\Api;


use App\Models\Airports;
use Illuminate\Http\Request;
use App\Models\AircraftTypes;
use App\Http\Controllers\Controller;

// customer
// email
// fuelCard
// acRegistration
// aircraftType
// departure
// arrival
// departureDate
// arrivalDate
// typeProduct
// gallons
// remarks
// flightOperation
// use Barryvdh\DomPDF\Facade as PDF;
// use App\Mail\SendPDF;
// set_time_limit(300);
// $pdf = PDF::loadView('pdfview',compact('FuelRequest'))->stream();


// Mail::to('zrapar@gmail.com')->send(new SendUnconfirmedPDF($FuelRequest,$pdf));
// set_time_limit(30);

class AirportController extends Controller
{
    public function getAirport()
    {
        try {
            return response()->json([
                'data' => Airports::paginate(),
                'success' => true
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'trace' => $e->getTrace(),
                'success' => false
            ], 500);
        }
    }

    public function getAircraft()
    {
        try {
            return response()->json([
                'data' => AircraftTypes::all(),
                'success' => true
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'trace' => $e->getTrace(),
                'success' => false
            ], 500);
        }
    }

    public function searchAirport(Request $request)
    {
        $array = [];
        try {
            $data = Airports::where('label', 'LIKE', '%' . $request->search_by . '%')->get();
            foreach ($data as $key => $value) {
                array_push($array, [
                    'label' => $value['label'],
                    'value' => $value['uuid']
                ]);
            }
            return response()->json($array, 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'trace' => $e->getTrace(),
                'success' => false
            ], 500);
        }
    }
}