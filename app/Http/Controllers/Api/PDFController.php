<?php

namespace App\Http\Controllers\Api;

use PDF;
use Validator;
use App\Models\User;
use App\Mail\SendPDF;
use Ramsey\Uuid\Uuid;
use App\Mail\SendLink;
use App\Models\Invoice;
use App\Models\FuelRequest;
use Illuminate\Http\Request;
use App\Models\AircraftTypes;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;


// customer
// email
// fuelCard
// acRegistration
// aircraftType
// departure
// arrival
// departureDate
// arrivalDate
// typeProduct
// gallons
// remarks
// flightOperation
// use Barryvdh\DomPDF\Facade as PDF;
// use App\Mail\SendPDF;
// set_time_limit(300);
// $pdf = PDF::loadView('pdfview',compact('FuelRequest'))->stream();


// Mail::to('johan.dise@gmail.com')->send(new SendUnconfirmedPDF($FuelRequest,$pdf));
// set_time_limit(30);

class PDFController extends Controller
{
  public function getDataForm(Request $request)
  {
    try {
      $rules = [

        'customer' => 'required',
        'email' => 'required',
        'acRegistration' => 'required',
        'aircraftType' => 'required',
        'departure' => 'required',
        'arrival' => 'required',
        'departureDate' => 'required',
        'departureTime' => 'required',
        'arrivalDate' => 'required',
        'arrivalTime' => 'required',
        'typeProduct' => 'required',
        'flightOperation' => 'required',
        'flightType' => 'required',
      ];

      $msg = [
        'customer.required' => 'No se envio el dueño de la areonave / the field "costumer" was not sent',
        'email.required' => 'No se envio el correo / the field "email" was not sent ',
        'acRegistration.required' => 'No se envio la matricula / the field "ac registration" was not sent',
        'aircraftType.required' => 'No se envio el tipo de aeronave / the field "aircraft type" was not sent',
        'departure.required' => 'No se envio la ciudad de salida / the field "departure city" was not sent',
        'arrival.required' => 'No se envio la ciudad de destino / the field "arrival city" was not sent',
        'departureDate.required' => 'No se envio la fecha de salida / the field "departure date" was not sent',
        'arrivalDate.required' => 'No se envio la fecha de llegada / / the field "arrival date" was not sent',
        'typeProduct.required' => 'No se envio el tipo de gasolina / the field "product" was not sent',
        'flightOperation.required' => 'No se envio el tipo de operacion / the field "flight operation" was not sent',
        'flightType.required' => 'No se envio el tipo de vuelo / the field "flight type" was not sent',
        'departureTime.required' => 'No se envio la hora de salida / the field "departure time" was not sent',
        'arrivalTime.required' => 'No se envio la hora de llegada / the field "arrival time" was not sent',
      ];

      if (isset($request->captainRequest) && $request->captainRequest != "captainRequest") {
        $rules['gallons'] = 'required';
        $msg['gallons.required'] = 'No se envio la cantidad de carga / the field "estimated uplift" was not sent';
      }

      $validator = Validator::make($request->all(), $rules, $msg);

      if ($validator->fails()) {
        return response()->json([
          'errors' => $validator->errors(),
          'success' => false
        ], 404);
      }

      $aircraftType = $request->aircraftType;

      $gallons = $request->gallons;

      if (isset($request->captainRequest) && $request->captainRequest == 'captainRequest') {
        $gallons = "At Captain's request";
      }

      $aircraft = AircraftTypes::where('uuid', $request->aircraftType)->first();

      if (!isset($aircraft)) {


        $air = AircraftTypes::create([
          'uuid' => Uuid::uuid4()->toString(),
          'type' => $request->aircraftType
        ]);

        $air->save();

        $aircraftType = $air->uuid;
      }

      $FuelRequest = FuelRequest::create([
        'uuid' => Uuid::uuid4()->toString(),
        'ac_registration' => $request->acRegistration,
        'aircraft_type' => $aircraftType,
        'arrival_code' => $request->arrival,
        'arrival_date' => $request->arrivalDate,
        'arrival_time' => $request->arrivalTime,
        'customer' => $request->customer,
        'departure_code' => $request->departure,
        'departure_date' => $request->departureDate,
        'departure_time' => $request->departureTime,
        'flight_operation' => $request->flightOperation,
        'flight_type' => $request->flightType,
        'gallons' => $gallons,
        'type_product' => $request->typeProduct,
        'email_costumer' => $request->email,
        'remarks' => null,
        'is_completed' => '0',
        'send_mail' => '0'
      ]);

      if (isset($request->handler)) {
        $FuelRequest->handler = $request->handler;
      }

      if (isset($request->remarks)) {
        $FuelRequest->remarks = $request->remarks;
      }

      if ($FuelRequest->save()) {
        $user = User::all();

        Mail::to($user[0]->email_to_send)->send(new SendLink($FuelRequest->uuid));

        return response()->json([
          'message' => 'Se genero una nueva solicitud',
          'success' => true
        ], 201);
      }
      return response()->json([
        'message' => 'Ha ocurrido un error',
        'success' => false
      ], 500);
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }

  public function postConfirmedData(Request $request)
  {
    try {
      $validator = Validator::make($request->all(), [
        'supplier' => 'required',
        'customer' => 'required',
        'from' => 'required',
        'uuidRequest' => 'required',
        'typeSend' => 'required',

      ], [
        'supplier.required' => 'the field "supplier" was not sent',
        'customer.required' => 'the field "customer owner" was not sent ',
        'from.required' => 'the field "from" was not sent',
        'uuidRequest.required' => 'not exist fuel request',
        'typeSend.required' => 'Dont send the type of send',
      ]);

      if ($validator->fails()) {
        return response()->json([
          'errors' => $validator->errors(),
          'success' => false
        ], 404);
      }


      $lastInvoiceNumber = Invoice::latest()->first();
      $correlative = str_random(6);

      if (isset($lastInvoiceNumber)) {
        if (isset($request->correlative) && $lastInvoiceNumber->id_invoice != $request->correlative && (strlen($request->correlative) > 0)) {
          $correlative = $request->correlative;
        }
      } else {
        if (isset($request->correlative) && (strlen($request->correlative) > 0)) {
          $correlative = $request->correlative;
        }
      }

      $Invoice = Invoice::create([
        'uuid' => Uuid::uuid4()->toString(),
        'id_invoice' => $correlative,
        'supplier' => $request->supplier,
        'owner' => $request->customer,
        'from' => $request->from,
        'request_uuid' => $request->uuidRequest,
      ]);

      $Invoice->save();

      $FuelRequest = FuelRequest::where('uuid', $request->uuidRequest)
        ->with(['arrival', 'departure', 'aircraft'])
        ->with(['invoice' => function ($e) {
          $e->with(['suppliers', 'clients', 'agencies']);
        }])->first();

      $FuelRequest->is_completed = '1';

      $FuelRequest->save();

      if ($request->typeSend) {

        $isSend = $this->sendMail($FuelRequest, $correlative);

        if ($isSend) {
          $FuelRequest->send_mail = '1';
          $FuelRequest->save();
          return response()->json([
            'message' => 'The Request has been confirmed, please check your e-mail with the details of request',
            'success' => true
          ], 201);
        }

        return $isSend;
      }
      return response()->json([
        'message' => 'The Request has been confirmed, but the requested email its not sended',
        'success' => true
      ], 201);
    } catch (\Exception $e) {

      return response()->json([
        'message' => $e->getMessage(),
        'trace' => json_encode($e->getTrace()),
        'success' => false
      ], 500);
    }
  }

  public function getDataToResponse(Request $request)
  {
    try {
      $FuelRequest = FuelRequest::where('uuid', $request->uuid)->with(['arrival', 'departure', 'aircraft'])->first();
      $lastInvoiceNumber = Invoice::latest()->first();
      return response()->json([
        'data' => $FuelRequest,
        'lastInvoiceNumber' => $lastInvoiceNumber,
        'success' => true
      ], 200);
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }

  public function deleteRequest(Request $request)
  {
    try {
      $FuelRequest = FuelRequest::where('uuid', $request->uuid)->first();
      if ($FuelRequest->delete()) {
        return response()->json([
          'message' => 'Se elimino correctamente la peticion',
          'success' => true
        ], 200);
      }
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }

  public function getRequests()
  {
    try {
      $FuelRequest = FuelRequest::with(['arrival', 'departure', 'invoice', 'aircraft'])->latest()->paginate(15);
      if (!$FuelRequest) {
        return response()->json([
          'message' => 'Error',
          'success' => false
        ], 401);
      }
      return response()->json([
        'data' => $FuelRequest,
        'success' => true
      ], 200);
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }
  public function seePdf($uuidRequest)
  {

    $FuelRequest = FuelRequest::where('uuid', $uuidRequest)
      ->with(['arrival', 'departure', 'aircraft'])
      ->with(['invoice' => function ($e) {
        $e->with(['suppliers', 'clients', 'agencies']);
      }])->first();
    $type = 'view';

    if ($FuelRequest->is_completed == '1' && isset($FuelRequest->invoice)) {
      return view('pdfweb', compact('FuelRequest', 'type'));
    }

    return redirect('/dashboard');
  }
  public function downloadPdf($uuidRequest)
  {

    $FuelRequest = FuelRequest::where('uuid', $uuidRequest)
      ->with(['arrival', 'departure', 'aircraft'])
      ->with(['invoice' => function ($e) {
        $e->with(['suppliers', 'clients', 'agencies']);
      }])->first();
    $type = 'view';

    if ($FuelRequest->is_completed == '1' && isset($FuelRequest->invoice)) {
      $name = "RELEASE JBP FO#";
      $name .= $FuelRequest->invoice->id_invoice . " ";
      $name .= $FuelRequest->invoice->clients->client . " ";
      $name .= $FuelRequest->departure->iata . " - " . $FuelRequest->departure->icao . " ";
      $name .= $FuelRequest->ac_registration . " ";
      $name .= "ETA " . Carbon::parse($FuelRequest->arrival_date)->format('M-d-Y');
      return PDF::loadView('pdfview', compact('FuelRequest'))->stream("{$name}.pdf");
    }

    return redirect('/dashboard');
  }

  public function updateRequest($uuid, Request $request)
  {
    try {
      $validator = Validator::make($request->all(), [
        'customer' => 'required',
        'emailCostumer' => 'required',
        'acRegistration' => 'required',
        'aircraftType' => 'required',
        'departureCode' => 'required',
        'arrivalCode' => 'required',
        'departureDate' => 'required',
        'departureTime' => 'required',
        'arrivalDate' => 'required',
        'arrivalTime' => 'required',
        'typeProduct' => 'required',
        'gallons' => 'required',
        'flightOperation' => 'required',
        'flightType' => 'required',
      ], [
        'customer.required' => 'No se envio el dueño de la areonave / the field "costumer" was not sent',
        'emailCostumer.required' => 'No se envio el correo / the field "email" was not sent ',
        'acRegistration.required' => 'No se envio la matricula / the field "ac registration" was not sent',
        'aircraftType.required' => 'No se envio el tipo de aeronave / the field "aircraft type" was not sent',
        'departureCode.required' => 'No se envio la ciudad de salida / the field "departure city" was not sent',
        'arrivalCode.required' => 'No se envio la ciudad de destino / the field "arrival city" was not sent',
        'departureDate.required' => 'No se envio la fecha de salida / the field "departure date" was not sent',
        'arrivalDate.required' => 'No se envio la fecha de llegada / / the field "arrival date" was not sent',
        'typeProduct.required' => 'No se envio el tipo de gasolina / the field "product" was not sent',
        'gallons.required' => 'No se envio la cantidad de carga / the field "estimated uplift" was not sent',
        'flightOperation.required' => 'No se envio el tipo de operacion / the field "flight operation" was not sent',
        'flightType.required' => 'No se envio el tipo de vuelo / the field "flight type" was not sent',
        'departureTime.required' => 'No se envio la hora de salida / the field "departure time" was not sent',
        'arrivalTime.required' => 'No se envio la hora de llegada / the field "arrival time" was not sent',
      ]);

      if ($validator->fails()) {
        return response()->json([
          'errors' => $validator->errors(),
          'success' => false
        ], 404);
      }

      $aircraftType = $request->aircraftType;

      $gallons = $request->gallons;

      if (isset($request->captainRequest) && $request->captainRequest == 'captainRequest') {
        $gallons = "At Captain's request";
      }

      $aircraft = AircraftTypes::where('uuid', $request->aircraftType)->first();

      if (!isset($aircraft)) {


        $air = AircraftTypes::create([
          'uuid' => Uuid::uuid4()->toString(),
          'type' => $request->aircraftType
        ]);

        $air->save();

        $aircraftType = $air->uuid;
      }


      $FuelRequest = FuelRequest::where('uuid', $uuid)->first();

      if (!$FuelRequest) {
        return response()->json([
          'message' => 'No existe esta solicitud',
          'success' => false
        ], 404);
      }


      $FuelRequest->fill([
        'ac_registration' => $request->acRegistration,
        'aircraft_type' => $aircraftType,
        'arrival_code' => $request->arrivalCode,
        'arrival_date' => $request->arrivalDate,
        'arrival_time' => $request->arrivalTime,
        'customer' => $request->customer,
        'handler' => null,
        'departure_code' => $request->departureCode,
        'departure_date' => $request->departureDate,
        'departure_time' => $request->departureTime,
        'flight_operation' => $request->flightOperation,
        'flight_type' => $request->flightType,
        'gallons' => $gallons,
        'type_product' => $request->typeProduct,
        'email_costumer' => $request->emailCostumer,
        'remarks' => null,
      ]);

      if (isset($request->handler)) {
        $FuelRequest->handler = $request->handler;
      }

      if (isset($request->remarks)) {
        $FuelRequest->remarks = $request->remarks;
      }

      if ($FuelRequest->update()) {
        return response()->json([
          'message' => 'Se actualizo la solicitud',
          'success' => true
        ], 201);
      }
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }

  private function sendMail($FuelRequest, $correlative)
  {
    try {
      $FuelRequest = json_decode($FuelRequest->toJson(JSON_UNESCAPED_UNICODE), JSON_UNESCAPED_UNICODE);
      unset($FuelRequest['uuid']);
      unset($FuelRequest['arrival']['uuid']);
      unset($FuelRequest['departure']['uuid']);
      unset($FuelRequest['aircraft']['uuid']);
      unset($FuelRequest['invoice']['uuid']);
      unset($FuelRequest['invoice']['suppliers']['uuid']);
      unset($FuelRequest['invoice']['clients']['uuid']);
      unset($FuelRequest['invoice']['agencies']['uuid']);


      set_time_limit(600);
      // $temp_file = tempnam(sys_get_temp_dir(), "Invoice-{$correlative}");
      // PDF::loadView('pdfview', compact('FuelRequest'))->save($temp_file);
      $pdf = PDF::loadView('pdfview', compact('FuelRequest'))->stream();

      $name = "RELEASE JBP FO#";
      $name .= $correlative . " ";
      $name .= $FuelRequest['invoice']['clients']['client'] . " ";
      $name .= $FuelRequest['departure']['iata'] . " - " . $FuelRequest['departure']['icao'] . " ";
      $name .= $FuelRequest['ac_registration'] . " ";
      $name .= "ETA " . Carbon::parse($FuelRequest['arrival_date'])->format('M-d-Y');

      $message = new SendPDF($FuelRequest, $pdf, $name);

      $arrayCC = [
        $FuelRequest['invoice']['suppliers']['supplier_email'],
        $FuelRequest['invoice']['clients']['client_email'],
        $FuelRequest['invoice']['agencies']['agency_email']
      ];

      if (isset($FuelRequest['invoice']['suppliers']['attn_email']) && $FuelRequest['invoice']['suppliers']['supplier_email'] != $FuelRequest['invoice']['suppliers']['attn_email']) {
        array_push($arrayCC, $FuelRequest['invoice']['suppliers']['attn_email']);
      }

      if (isset($FuelRequest['invoice']['suppliers']['cc_email']) && $FuelRequest['invoice']['suppliers']['supplier_email'] != $FuelRequest['invoice']['suppliers']['cc_email']) {
        array_push($arrayCC, $FuelRequest['invoice']['suppliers']['cc_email']);
      }

      if (isset($FuelRequest['invoice']['suppliers']['attn_email']) && $FuelRequest['invoice']['suppliers']['supplier_email'] != $FuelRequest['invoice']['suppliers']['attn_email']) {
        array_push($arrayCC, $FuelRequest['invoice']['clients']['attn_email']);
      }

      if (isset($FuelRequest['invoice']['clients']['cc_email']) && $FuelRequest['invoice']['clients']['client_email'] != $FuelRequest['invoice']['clients']['cc_email']) {
        array_push($arrayCC, $FuelRequest['invoice']['clients']['cc_email']);
      }

      if (isset($FuelRequest['invoice']['agencies']['attn_email']) && $FuelRequest['invoice']['agencies']['agency_email'] != $FuelRequest['invoice']['agencies']['attn_email']) {
        array_push($arrayCC, $FuelRequest['invoice']['agencies']['attn_email']);
      }

      if (isset($FuelRequest['invoice']['agencies']['cc_email']) && $FuelRequest['invoice']['agencies']['agency_email'] != $FuelRequest['invoice']['agencies']['cc_email']) {
        array_push($arrayCC, $FuelRequest['invoice']['agencies']['cc_email']);
      }

      foreach ($arrayCC as $key => $email) {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
          unset($arrayCC[$key]);
        }
      }

      Mail::to($FuelRequest['email_costumer'])->bcc($arrayCC)->send($message);

      set_time_limit(30);
      return true;
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }

  public function sendMailFromDashboard($uuid)
  {
    $FuelRequest = FuelRequest::where('uuid', $uuid)
      ->with(['arrival', 'departure', 'aircraft'])
      ->with(['invoice' => function ($e) {
        $e->with(['suppliers', 'clients', 'agencies']);
      }])->first();

    $correlative = $FuelRequest['invoice']['id_invoice'];
    $isSend = $this->sendMail($FuelRequest, $correlative);

    if ($isSend) {
      $FuelRequest->send_mail = '1';
      $FuelRequest->save();
      return response()->json([
        'message' => 'The Request has been confirmed, please check your e-mail with the details of request',
        'success' => true
      ], 201);
    }

    return $isSend;
  }
}