<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
  public function update(Request $request, $id)
  {
    try {
      $user = User::findOrFail($id);
      $data = $request->all();
      if (array_key_exists('password', $data)) {
        $data['password'] = bcrypt($data['password']);
        $user->update($data);
        return response()->json([
          'user' => $user,
          'message' => 'Perfil actualizado correctamente',
          'success' => true
        ], 201);
      }

      $user->update($data);
      return response()->json([
        'user' => $user,
        'message' => 'Perfil actualizado correctamente',
        'success' => true
      ], 201);
    } catch (\Exception $e) {
      return response()->json([
        'message' => 'Ha ocurrido un error actualizando',
        'success' => false
      ], 404);
    }
  }
}