<?php

namespace App\Http\Controllers\Api;

use Validator;
use Ramsey\Uuid\Uuid;
use App\Models\Agencies;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgenciesController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    try {
      $Agencies = Agencies::with(['invoices'])->paginate(15);
      if (!$Agencies) {
        return response()->json([
          'message' => 'Error',
          'success' => false
        ], 401);
      }
      return response()->json([
        'data' => $Agencies,
        'success' => true
      ], 200);
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {

    try {
      $rules = [
        'agency' => 'required',
        'agencyEmail' => ['required', 'regex:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/']
      ];

      $msg = [
        'agency.required' => 'No se envio el dueño de la areonave / the field "agency" was not sent',
        'agencyEmail.required' => 'No se envio el correo / the field "agency email" was not sent ',
        'agencyEmail.regex' => 'Este correo es invalido / this email its invalid'
      ];

      if (isset($request->attnEmail)) {
        $rules['attnEmail'] = ['regex:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'];
        $msg['attnEmail.regex'] = 'Este correo es invalido / this email its invalid';
      }

      if (isset($request->ccEmail)) {
        $rules['ccEmail'] = ['regex:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'];
        $msg['ccEmail.regex'] = 'Este correo es invalido / this email its invalid';
      }

      $validator = Validator::make($request->all(), $rules, $msg);

      if ($validator->fails()) {
        return response()->json([
          'errors' => $validator->errors(),
          'success' => false
        ], 404);
      }

      $Agencies = Agencies::create([
        'uuid' => Uuid::uuid4()->toString(),
        'agency' => $request->agency,
        'agency_email' => $request->agencyEmail
      ]);

      if (isset($request->attnEmail)) {
        $Agencies['attn_email'] = $request->attnEmail;
      }

      if (isset($request->ccEmail)) {
        $Agencies['cc_email'] = $request->ccEmail;
      }

      if ($Agencies->save()) {

        return response()->json([
          'message' => 'Se genero una nueva agencia',
          'success' => true,
          'data' => $Agencies
        ], 201);
      }
      return response()->json([
        'message' => 'Ha ocurrido un error',
        'success' => false
      ], 500);
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\Agencies  $agencies
   * @return \Illuminate\Http\Response
   */
  public function show(Agencies $agencies)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Models\Agencies  $agencies
   * @return \Illuminate\Http\Response
   */
  public function getAll()
  {
    try {
      $Agencies = Agencies::all();
      if (!$Agencies) {
        return response()->json([
          'message' => 'Error',
          'success' => false
        ], 401);
      }
      return response()->json([
        'data' => $Agencies,
        'success' => true
      ], 200);
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\Agencies  $agencies
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Agencies $agencies)
  {
    try {
      $Agency = $agencies->where('uuid', $request->uuid)->first();
      if (!$Agency) {
        return response()->json([
          'message' => 'No existe la agencia asociada',
          'success' => false
        ], 404);
      }

      $rules = [
        'agency' => 'required',
        'agencyEmail' => ['required', 'regex:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/']
      ];

      $msg = [
        'agency.required' => 'No se envio el dueño de la areonave / the field "agency" was not sent',
        'agencyEmail.required' => 'No se envio el correo / the field "agency email" was not sent ',
        'agencyEmail.regex' => "Ya existe este correo en la base de datos / This agency's email exists ",
      ];

      if (isset($request->attnEmail) && $Agency->attn_email != $request->attnEmail) {
        $rules['attnEmail'] = ['regex:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'];
        $msg['attnEmail.regex'] = 'Este correo es invalido / this email its invalid';
      }

      if (isset($request->ccEmail) && $Agency->cc_email != $request->ccEmail) {
        $rules['ccEmail'] = ['regex:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'];
        $msg['ccEmail.regex'] = 'Este correo es invalido / this email its invalid';
      }

      $validator = Validator::make($request->all(), $rules, $msg);

      if ($validator->fails()) {
        return response()->json([
          'errors' => $validator->errors(),
          'success' => false
        ], 404);
      }

      $Agency->fill([
        'client' => $request->agency,
        'client_email' => $request->agencyEmail,
      ]);

      if (isset($request->attnEmail)) {
        $Agency['attn_email'] = $request->attnEmail;
      }
      if (isset($request->ccEmail)) {
        $Agency['cc_email'] = $request->ccEmail;
      }

      if ($Agency->save()) {
        return response()->json([
          'message' => 'La Agencia fue actualizada correctamente',
          'success' => true,
          'data' => $Agency
        ], 201);
      }
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\Agencies  $agencies
   * @return \Illuminate\Http\Response
   */
  public function destroy($uuid, Agencies $agencies)
  {
    try {
      $Agency = $agencies->where('uuid', $uuid)->first();
      if (!$Agency) {
        return response()->json([
          'message' => 'No existe la agencia asociada',
          'success' => false
        ], 404);
      }

      $Agency->forceDelete();

      return response()->json([
        'message' => 'La Agencia fue borrada correctamente',
        'success' => true,
      ], 200);
    } catch (\Exception $e) {
      return response()->json([
        'message' => $e->getMessage(),
        'trace' => $e->getTrace(),
        'success' => false
      ], 500);
    }
  }
}