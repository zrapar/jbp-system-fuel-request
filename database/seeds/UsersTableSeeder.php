<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    \App\Models\User::create([
      'name' => 'Admin Account',
      'email' => 'admin@admin.com',
      'email_to_send' => 'admin@admin.com',
      'password' => bcrypt('123456789'),
      'is_admin' => true,
      'remember_token' => str_random(10),
    ]);
  }
}