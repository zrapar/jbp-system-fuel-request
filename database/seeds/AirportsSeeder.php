<?php

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Seeder;
use App\Models\Airports;

class AirportsSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Storage::delete('filter-json.json');
    Artisan::call('airports:transform');
    $json = json_decode(Storage::get('filter-json.json'), true);
    foreach ($json as $key => $data) {
      Airports::create([
        'uuid' => $data['uuid'],
        'label' => $data['label'],
        'iata' => $data['iata_code'],
        'icao' => $data['icao_code'],
        'type' => $data['type'],
      ])->save();
    }
  }
}