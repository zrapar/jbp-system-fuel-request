<?php

use Ramsey\Uuid\Uuid;
use App\Models\AircraftTypes;
use Illuminate\Database\Seeder;


class AircraftTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = json_decode(Storage::get('aircraft-type.json'), true)['aircraft'];
        foreach ($json as $key => $data) {
            AircraftTypes::create([
                'uuid' => Uuid::uuid4()->toString(),
                'type' => $data,
            ])->save();
        }
    }
}