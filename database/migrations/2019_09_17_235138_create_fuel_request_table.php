<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuelRequestTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('fuel_request', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('ac_registration');
      $table->string('uuid');
      $table->string('aircraft_type');
      $table->string('arrival_code');
      $table->date('arrival_date');
      $table->string('arrival_time');
      $table->string('customer');
      $table->string('handler')->default(null)->nullable(true);
      $table->string('departure_code');
      $table->date('departure_date');
      $table->string('departure_time');
      $table->enum('flight_operation', ['corporative', 'charter']);
      $table->enum('flight_type', ['international', 'domestic']);
      $table->string('gallons');
      $table->string('remarks')->default(null)->nullable(true);
      $table->enum('type_product', ['Jet A1', 'AV Gas']);
      $table->string('email_costumer');
      $table->enum('is_completed', ['0', '1'])->default('0');
      $table->enum('send_mail', ['0', '1'])->default('0');
      $table->softDeletes();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('fuel_request');
  }
}