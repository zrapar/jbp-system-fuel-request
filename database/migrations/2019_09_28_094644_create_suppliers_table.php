<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid');
            $table->string('supplier');
            $table->string('supplier_email');
            $table->string('attn')->default(null)->nullable(true);
            $table->string('attn_email')->default(null)->nullable(true);
            $table->string('cc')->default(null)->nullable(true);
            $table->string('cc_email')->default(null)->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}